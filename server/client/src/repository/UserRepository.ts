import { authToken } from "../resources/constants"

class UserRepository {

    createHeadersWithParams(params: any): any {
        const heads = this.createHeaders()
        heads["params"] = params
        console.log(heads)
        return heads;
    }

    createHeadersWithMultiPart(): any {
        const heads = {
            headers: {
                authorization: localStorage.getItem(authToken),
                'Content-Type': 'multipart/form-data'
            }
        }

        return heads
    }

    createHeaders(): any {
        const heads = {
            headers: {
                authorization: localStorage.getItem(authToken),
            }
        }
        return heads
    }
}

export default UserRepository