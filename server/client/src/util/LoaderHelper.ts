import { LoadingState, PageState } from "../model/data/states"
import { ClipLoader } from "react-spinners";

export const getCountComponent = (pageState: PageState) => {
    if (pageState instanceof LoadingState) {
        return '<ClipLoader className="m" color = "#428bca" size = {24} />'
    } else {
        return '<h3 className="text-primary" > { props.count } < /h3>'
    }
}

