import { FormControls } from "../model/formcontrols/FormControls";
import { SelectInput, MultiSelectInput } from "../model/formcontrols/FormTextInput";

export const updateFormControls = (name: string, value: string, formControls: FormControls): FormControls => {
    const updatedControls: FormControls = { ...formControls }
    const updatedFormElement = { ...updatedControls[name] }

    updatedFormElement.value = value;
    updatedFormElement.isValid = updatedFormElement.validationRule.validate(value)
    updatedControls[name] = updatedFormElement;

    return updatedControls
}

export const updateFormControlsSelect = (name: string, key: string, value: string, formControls: FormControls): FormControls => {
    const updatedControls: FormControls = { ...formControls }
    const updatedFormElement = { ...updatedControls[name] } as SelectInput

    updatedFormElement.key = key
    updatedFormElement.value = value;
    updatedFormElement.isValid = updatedFormElement.validationRule.validate(value)
    updatedControls[name] = updatedFormElement;

    return updatedControls
}

export const updateFormControlsMultiSelect = (
    selectedOption: any,
    action: any,
    formControls: FormControls
): FormControls => {
    switch (action.action) {
        case "select-option":
            return addOption(selectedOption, action, formControls);
        case "remove-value":
            return removeOption(action, formControls)
        case "clear":
            return clearOptions(action, formControls)
        default: return formControls
    }
}

const addOption = (selectedOption: any, action: any, formControls: FormControls): FormControls => {
    const name = action.name
    const selectedKeys = selectedOption.map((option: any) => option.value);
    const selectedValues = selectedOption.map((option: any) => option.label);
    
    const updatedControls: FormControls = { ...formControls };
    const updatedFormElement = { ...updatedControls[name] } as MultiSelectInput;

    updatedFormElement.keys = selectedKeys;
    updatedFormElement.value = selectedValues;
    updatedFormElement.isValid = updatedFormElement.validationRule.validate(selectedValues);
    updatedControls[name] = updatedFormElement;

    return updatedControls;
}

const removeOption = (action: any, formControls: FormControls): FormControls => {
    const name = action.name
    const removedValueKey = action.removedValue.value

    const updatedControls: FormControls = { ...formControls };
    const updatedFormElement = { ...updatedControls[name] } as MultiSelectInput;

    let indexToRemove: number = -1
    updatedFormElement.keys.forEach((key, index) => {
        console.log(key + " === " + removedValueKey)
        if (key.localeCompare(removedValueKey) === 0) {
            indexToRemove = index
        }
    })

    if (indexToRemove != -1) {
        updatedFormElement.keys.splice(indexToRemove, 1);
        updatedFormElement.value.splice(indexToRemove, 1);
        updatedFormElement.isValid = updatedFormElement.validationRule.validate(updatedFormElement.value);
        updatedControls[name] = updatedFormElement;
        return updatedControls
    } else {
        return formControls
    }
}

const clearOptions = (action: any, formControls: FormControls): FormControls => {
    const name = action.name
    
    const updatedControls: FormControls = { ...formControls };
    const updatedFormElement = { ...updatedControls[name] } as MultiSelectInput;

    updatedFormElement.keys = [];
    updatedFormElement.value = [];
    updatedFormElement.isValid = updatedFormElement.validationRule.validate(updatedFormElement.value);
    updatedControls[name] = updatedFormElement;

    return updatedControls;
}

export const validateFormControls = (updatedControls: FormControls): boolean => {
    let formValid = true;
    for (let inputId in updatedControls) {
        formValid = updatedControls[inputId].isValid && formValid;

        if (formValid == false) break;
    }

    return formValid;
};