export const getSelectStyles = (isValid: boolean, applyStyles: boolean) => {
    return {
        control: (base: any) => ({
            ...base,
            borderColor: (isValid || !applyStyles) ? "lightgray" : "red"
        })
    }
}

export const getInvalidFeedBackStyle = (isValid: boolean, applyStyles: boolean) => {
    return "invalid-feedback " + (isValid || !applyStyles ? "" : "d-block")
}

export const getFormControlStyles = (isValid: boolean, applyStyles: boolean) => {
    return "form-control " + (isValid || !applyStyles ? "" : "is-invalid")
}