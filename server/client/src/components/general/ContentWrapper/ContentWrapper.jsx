import React from "react";

import "./ContentWrapper.css";

const ContentWrapper = props => {
    return (
        <div
            className={
                "wrapper " + (props.isSideBarShown ? "" : "wrapper-toggle")
            }
        >
            <div className="shadow p-3 m-4 bg-white rounded Produtcs-wrapper">
                {props.children}
            </div>
        </div>
    );
};

export default ContentWrapper;
