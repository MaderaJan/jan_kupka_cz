import React from 'react';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";

const ItemAction = props => {

    return (
        <div className="d-block">

            <button className="btn btn-success mr-2" onClick={props.onEdit}>
                <FontAwesomeIcon icon={faEdit} color="FFF" />
            </button>

            <button className="btn btn-danger" onClick={props.onDelete}>
                <FontAwesomeIcon icon={faTrash} color="FFF" />
            </button>
            
        </div>
    );
}

export default ItemAction;