import React from "react";
import ClipLoader from "react-spinners/ClipLoader"

function WithLoading(Component) {
    return function WihLoadingComponent({ isLoading, ...props }) {

        if (!isLoading) return (<Component {...props} />);

        return (
            <div className="row d-flex justify-content-center">
                <div className="col-md-1">
                    <ClipLoader className="m" color="#428bca" size={props.size ?? 75} />
                  </div>
            </div>
        )
    }
}

export default WithLoading;