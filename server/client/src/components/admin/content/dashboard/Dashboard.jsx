import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import DashboardProductTile from "./tile/DashboardProductTile";
import DashboardWarehouseTile from "./tile/DashboardWarehouseTile";

import { fetchData } from "./../../../../redux/action/DashboardActions"

const Dashboard = (props) => {

    useEffect(() => {
        props.fetchData()        
    }, [])

    return (
        <div>
            <div className="row justify-content-center">
                <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                    <DashboardProductTile onlineCount={props.dashboardInfo.productOnline} offlineCount={props.dashboardInfo.productOffline} />
                </div>

                <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                    <DashboardWarehouseTile count={props.dashboardInfo.warehouseItemsCounts}/>
                </div>
            </div>

        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        pageState: state.dashboardReducer.pageState,
        dashboardInfo: state.dashboardReducer.dashboardInfo
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: () => dispatch(fetchData()),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard));