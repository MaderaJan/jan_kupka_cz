import React from "react";
import { connect } from "react-redux";
import { LoadingState } from "../../../../../model/data/states";

import strings from "../../../../../resources/strings"
import WithLoading from "../../../../hoc/WithLoading";

const DashboardWarehouseTile = (props) => {

    const WarehouseCount = () => <h3 className="text-primary">{props.count}</h3>
    const WarehouseCountWithLoading = WithLoading(WarehouseCount)

    return (
        <div className="card">
            <div className="card-body">
                <h5 className="card-title mb-3">{strings.warehouse}</h5>
                <h6 className="card-subtitle mb-2 text-muted">{strings.total}</h6>
                <WarehouseCountWithLoading {...props} isLoading={props.pageState instanceof LoadingState} size={24} />
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        pageState: state.dashboardReducer.pageState,
    }
}

export default connect(mapStateToProps, null)(DashboardWarehouseTile)