import React from "react";
import { LoadingState } from "../../../../../model/data/states";
import strings from "../../../../../resources/strings"
import { connect } from "react-redux";

import WithLoading from "../../../../hoc/WithLoading";

const DashboardProductTile = (props) => {

    const OnlineCount = () => <h3 className="text-primary">{props.onlineCount}</h3>
    const OnlineCountWithLoading = WithLoading(OnlineCount)

    const OfflineCount = () => <h3 className="text-primary">{props.offlineCount}</h3>
    const OfflineCountWithLoading = WithLoading(OfflineCount)

    return (
        <div className="card">
            <div className="card-body">
                <h5 className="card-title mb-3">{strings.products}</h5>
                <div className="row">
                    <div className="col-6">
                        <h6 className="card-subtitle mb-2 text-muted text-truncate">{strings.displayed}</h6>
                        <OnlineCountWithLoading isLoading={props.pageState instanceof LoadingState} size={24} />
                    </div>
                    <div className="col-6">
                        <h6 className="card-subtitle mb-2 text-muted text-truncate">{strings.not_displayed}</h6>
                        <OfflineCountWithLoading isLoading={props.pageState instanceof LoadingState} size={24} />
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        pageState: state.dashboardReducer.pageState,
    }
}

export default connect(mapStateToProps, null)(DashboardProductTile)