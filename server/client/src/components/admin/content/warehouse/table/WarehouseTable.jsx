import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux"

import { deleteWarehouseItem } from "../../../../../redux/action/WarehouseActions"

import WarehouseTableRow from "./WarehouseTableRow";
import strings from "../../../../../resources/strings";

const WarehouseTable = (props) => {

    const handleDeleteItem = id => {
        props.deleteWarehouseItem(id);
    };

    const handleEditItem = id => {
        props.history.push("/admin/warehouse/detail/" + id)
    }

    return (
        <React.Fragment>

            <div className="table-responsive-sm">
                <table className="table shadow-sm">
                    <thead>
                        <tr>
                            <th>{strings.name}</th>
                            <th>{strings.price}</th>
                            <th>{strings.amount}</th>
                            <th>{strings.type}</th>
                            <th>{strings.unit}</th>
                            <th>{strings.action}</th>
                        </tr>
                    </thead>

                    <tbody>
                        {props.items.map(item => (
                            <WarehouseTableRow item={item} onDelete={handleDeleteItem} onEdit={handleEditItem} />
                        ))}
                    </tbody>
                </table>
            </div>
        </React.Fragment>
    );
};

const mapStateToProps = state => {
    return {
        items: state.warehouseReducer.items,
        pageState: state.warehouseReducer.pageState
    }
}

const mapDispatchToProps = dispatch => {
    return {
        deleteWarehouseItem: id => dispatch(deleteWarehouseItem(id))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WarehouseTable));
