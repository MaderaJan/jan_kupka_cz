import React from "react";

import ItemAction from "../../../../general/ItemAction/ItemAction";
import strings from "../../../../../resources/strings";

const WarehouseTableRow = props => {

    const getType = type => {
        return strings.item_type.find(item => item.code === type.code).name;
    };

    return (
        <tr itemScope="row">
            <td>
                <span>{props.item.name}</span>
            </td>

            <td>
                <span>{props.item.cost}</span>
            </td>

            <td>
                <span>{props.item.amount}</span>
            </td>

            <td>
                <span>{getType(props.item.type)}</span>
            </td>

            <td>
                <span>{props.item.unit}</span>
            </td>

            <td>
                <ItemAction onDelete={() => props.onDelete(props.item._id)} onEdit={() => props.onEdit(props.item._id)} />
            </td>
        </tr>
    );
};

export default WarehouseTableRow;
