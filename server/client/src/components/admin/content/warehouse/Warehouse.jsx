import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

import { fetchWarehouseItems } from "../../../../redux/action/WarehouseActions"

import strings from "../../../../resources/strings";
import WithLoading from "../../../hoc/WithLoading";
import WarehouseTable from "./table/WarehouseTable";
import { LoadingState } from "../../../../model/data/states";

const Warehouse = (props) => {
    useEffect(() => {
        props.fetchWarehouseItems()
    }, []);

    const createNewItem = () => {
        props.history.push("/admin/warehouse/detail");
    };

    const WarehouseTableWithLoading = WithLoading(WarehouseTable)

    return (
        <React.Fragment>
            <button className="btn btn-primary mb-4" onClick={createNewItem}>
                <FontAwesomeIcon className="mr-2" icon={faPlus} />
                {strings.new_item}
            </button>

            <WarehouseTableWithLoading {...props} isLoading={props.pageState instanceof LoadingState}/>

        </React.Fragment>
    );
};

const mapStateToProps = state => {
    return {
        items: state.warehouseReducer.items,
        pageState: state.warehouseReducer.pageState
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchWarehouseItems: () => dispatch(fetchWarehouseItems()),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Warehouse));
