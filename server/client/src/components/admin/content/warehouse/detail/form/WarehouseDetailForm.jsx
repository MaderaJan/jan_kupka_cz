import React, { useState, useEffect } from "react";
import Select from "react-select";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getSelectStyles, getInvalidFeedBackStyle, getFormControlStyles} from "../../../../../../util/StylesHelper"

import strings from "../../../../../../resources/strings";
import "./WarehouseDetailForm.css";

import units from "../../../../../../model/data/unit"
import { PrepareState, SuccessState } from "../../../../../../model/data/states"
import { createWarehouseItem, validateForm, fetchWarehouseItem, validateFromSelect } from "../../../../../../redux/action/WarehouseDetailActions"
import PageNavigation from "../../../../../../resources/pageNavigation";

const WarehouseDetailForm = props => {
    const [applyStyles, setApplyStyles] = useState(false);

    useEffect(() => {
        if (props.pageState instanceof PrepareState && props.id) {
            props.fetchWarehouseItem(props.id)
        } else if (props.pageState instanceof SuccessState) {
            props.history.push(PageNavigation.WAREHOUSE_LIST)
        }
    })

    const formHandler = event => {
        const name = event.target.name;
        let value = event.target.value;

        if (event.target.options) {
            const selectedIndex = event.target.options.selectedIndex;
            value = event.target.options[selectedIndex].getAttribute("data-key");
        }

        props.validateForm(name, value, props.formControls)
    };

    const formHandleSelect = (selectedOption, action) => {
        props.validateFromSelect(action.name, selectedOption.value, selectedOption.label, props.formControls)
    }

    const createNewItem = () => {
        if (props.isFormValid) {
            props.createWarehouseItem(props.formControls, props.isInEditMode)
        } else {
            setApplyStyles(true)
        }
    };

    return (
        <form className="single-column-form mt-4">
            <div className="form-group">
                <label>{strings.name}</label>
                <input
                    value={props.formControls.name.value}
                    onChange={formHandler}
                    name="name"
                    type="text"
                    className={getFormControlStyles(props.formControls.name.isValid, applyStyles)}
                />
                <div className="invalid-feedback">
                    {strings.error_fill_field}
                </div>
            </div>

            <div className="form-row">
                <div className="col">
                    <div className="form-group">
                        <label>{strings.price}</label>
                        <input
                            value={props.formControls.cost.value}
                            onChange={formHandler}
                            name="cost"
                            type="number"
                            min="0"
                            className={getFormControlStyles(props.formControls.cost.isValid, applyStyles)}
                        />
                        <div className="invalid-feedback">
                            {strings.error_fill_field}
                        </div>
                    </div>
                </div>

                <div className="col">
                    <div className="form-group">
                        <label>{strings.type}</label>
                        <Select
                            name="type"
                            onChange={formHandleSelect}
                            styles={getSelectStyles(props.formControls.type.isValid, applyStyles)}
                            value={
                                {
                                    value: props.formControls.type.key,
                                    label: props.formControls.type.value
                                }
                            }
                            options={props.itemTypes.map(type => {
                                return {
                                    value: type.code,
                                    label: type.name
                                }
                            })} />
                        <div className={getInvalidFeedBackStyle(props.formControls.type.isValid, applyStyles)}>
                            {strings.error_choose_one}
                        </div>
                    </div>
                </div>
            </div>

            <div className="form-row">
                <div className="col">
                    <div className="form-group">
                        <label>{strings.amount}</label>
                        <input
                            value={props.formControls.amount.value}
                            onChange={formHandler}
                            name="amount"
                            type="number"
                            min="0"
                            className={getFormControlStyles(props.formControls.amount.isValid, applyStyles)}
                        />
                        <div className="invalid-feedback">
                            {strings.error_fill_field}
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="form-group">
                        <label>{strings.unit}</label>
                        <Select
                            name="unit"
                            onChange={formHandleSelect}
                            styles={getSelectStyles(props.formControls.type.isValid, applyStyles)}
                            value={
                                {
                                    value: props.formControls.unit.key,
                                    label: props.formControls.unit.value
                                }
                            }
                            options={units} />
                        <div className={getInvalidFeedBackStyle(props.formControls.unit.isValid, applyStyles)}>
                            {strings.error_choose_one}
                        </div>
                    </div>
                </div>
            </div>

            <hr className="m-0 mb-2 mt-1" />
            <button type="button" className="btn btn-primary w-25 p-2 mt-2 mr-2" onClick={() => createNewItem()}        >
                {props.isInEditMode ? strings.edit : strings.create}
            </button>
        </form>
    );
};

const mapStateToProps = (state) => {
    return {
        pageState: state.warehouseItemReducer.pageState,
        itemTypes: state.warehouseItemReducer.types,
        isFormValid: state.warehouseItemReducer.isFormValid,
        formControls: state.warehouseItemReducer.formControls,
        isInEditMode: state.warehouseItemReducer.isInEditMode
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        validateForm: (name, value, formControls) => dispatch(validateForm(name, value, formControls)),
        validateFromSelect: (name, key, value, formControls) => dispatch(validateFromSelect(name, key, value, formControls)),
        createWarehouseItem: (formControls, shouldModify) => dispatch(createWarehouseItem(formControls, shouldModify)),
        fetchWarehouseItem: (id) => dispatch(fetchWarehouseItem(id)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WarehouseDetailForm));