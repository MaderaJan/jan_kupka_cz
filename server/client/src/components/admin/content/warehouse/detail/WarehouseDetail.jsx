import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { fetchWareHouseItemTypes,  clearReducerState } from "../../../../../redux/action/WarehouseDetailActions"
import WithLoading from "../../../../hoc/WithLoading";
import WarehouseItemFrom from "./form/WarehouseDetailForm";
import { LoadingState } from "../../../../../model/data/states";

const WarehouseDetail = props => {

    const WarehouseDetailFormWithLoading = WithLoading(WarehouseItemFrom)

    useEffect(() => {
        props.fetchWareHouseItemTypes()
        
        return () => { 
            props.clearReducerState()
         }
    }, [])

    return (
        <WarehouseDetailFormWithLoading {...props} isLoading={props.pageState instanceof LoadingState}/>
    )
};

const mapStateToProps = (state) => {
    return {
        pageState: state.warehouseItemReducer.pageState,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchWareHouseItemTypes: () => dispatch(fetchWareHouseItemTypes()),
        clearReducerState: () => dispatch(clearReducerState())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WarehouseDetail));