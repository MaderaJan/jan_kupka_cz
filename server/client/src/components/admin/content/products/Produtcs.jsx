import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faSave } from "@fortawesome/free-solid-svg-icons";
import { fetchProducts, updateProductStatuses } from "../../../../redux/action/ProductListActions"
import strings from "../../../../resources/strings";
import WithLoading from "../../../hoc/WithLoading";
import ProductsTable from "./table/ProductsTable";
import { LoadingState, ModifiedState } from "../../../../model/data/states";

const Produtcs = props => {

    const createNewProduct = () => {
        props.history.push("/admin/products/detail");
    };

    const updateProductStatuses = () => {
        props.updateProductStatuses()
    }

    const showModifiedButton = () => {
        return props.pageState instanceof ModifiedState ?
            <button className="btn btn-primary mb-4 ml-4" onClick={updateProductStatuses}>
                <FontAwesomeIcon className="mr-2" icon={faSave} />
                {strings.save_changes}
            </button> : null
    }

    useEffect(() => {
        props.fetchProducts()
    }, []);

    const ProductsTableWithLoading = WithLoading(ProductsTable)

    return (
        <React.Fragment>
            
            <button className="btn btn-primary mb-4" onClick={createNewProduct}>
                <FontAwesomeIcon className="mr-2" icon={faPlus} />
                {strings.new_product}
            </button>

            {showModifiedButton()}

            <ProductsTableWithLoading {...props} isLoading={props.pageState instanceof LoadingState} />

        </React.Fragment>
    );
};

const mapStateToProps = (state) => {
    return {
        pageState: state.productListReducer.pageState
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchProducts: () => dispatch(fetchProducts()),
        updateProductStatuses: (chagedStatuses) => dispatch(updateProductStatuses(chagedStatuses))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Produtcs));
