import React from 'react';
import Select from 'react-select';

import './ProductRow.css';

import strings from '../../../../../../resources/strings';
import ItemAction from '../../../../../general/ItemAction/ItemAction';

const ProductTableRow = props => {

    const getType = type => {
        return strings.product_type.find(item => item.code === type.type).name;
    };

    const mapProductStatusToAppStatus = status => {
        const name = status ? strings.product_status.find(item => item.code == status).name : null

        if (name != null) {
            return {
                value: status,
                label: name,
            }
        } else {
            return {
                value: strings.product_status[0].code,
                label: strings.product_status[0].name,
            }
        }
    }

    const handleOnStatusChanged = (selectedOption, action) => {
        props.onStatusChanged(props.product._id, selectedOption, action)
    };

    return (
        <tr itemScope="row">
            <td className="image-cell">
                <img className="image-item" src={props.product.logo} alt="" />
            </td>

            <td className="align-middle">
                <span className="align-middle">{props.product.name}</span>
            </td>

            <td className="align-middle">
                <span>{props.product.avgWorkTime}</span>
            </td>

            <td className="align-middle">
                <span>{getType(props.product.type)}</span>
            </td>

            <td className="align-middle">
                <span>{props.product.sellPrice}</span>
            </td>

            <td className="align-middle">
                <Select
                    name="status"
                    onChange={handleOnStatusChanged}
                    options={strings.product_status.map(status => {
                        return {
                            value: status.code,
                            label: status.name
                        }
                    })}
                    value={mapProductStatusToAppStatus(props.product.status)}
                />
            </td>

            <td className="align-middle">
                <ItemAction onEdit={() => props.onEdit(props.product._id)} onDelete={() => props.onDelete(props.product._id)} />
            </td>
        </tr>
    )
}

export default ProductTableRow;