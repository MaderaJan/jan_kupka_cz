import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import ProductTableRow from "./row/ProductTableRow";

import { deleteProduct, changeProductStatus } from "../../../../../redux/action/ProductListActions"
import strings from "../../../../../resources/strings";

const ProductsTable = props => {

    const handleDeleteProduct = id => {
        props.deleteProduct(id);
    };

    const handleEditProduct = id => {
        props.history.push("/admin/products/detail/" + id)
    };

    const handleOnStatusChanged = (id, selectedOption, _) => {
        props.changeProductStatus(id, selectedOption.value)
    };

    return (
        <React.Fragment>
            <div className="table-responsive-sm">
                <table className="table shadow-sm">
                    <thead>
                        <tr>
                            <th>{strings.image}</th>
                            <th>{strings.name}</th>
                            <th>{strings.work_time}</th>
                            <th>{strings.type}</th>
                            <th>{strings.price}</th>
                            <th>{strings.state}</th>
                            <th>{strings.action}</th>
                        </tr>
                    </thead>

                    <tbody>
                        {props.products.map(product => (
                            <ProductTableRow product={product} onEdit={handleEditProduct} onDelete={handleDeleteProduct} onStatusChanged={handleOnStatusChanged}/>
                        ))}
                    </tbody>
                </table>
            </div>
        </React.Fragment>
    );
};

const mapStateToProps = (state) => {
    return {
        products: state.productListReducer.items
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteProduct: (id) => dispatch(deleteProduct(id)),
        changeProductStatus: (id, status) => dispatch(changeProductStatus(id, status))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductsTable));
