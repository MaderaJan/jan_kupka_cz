import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux"

import { initData, clearReducerState } from "../../../../../redux/action/ProductActions"
import WithLoading from "../../../../hoc/WithLoading";
import ProductForm from "./ProductForm";
import { LoadingState } from "../../../../../model/data/states";

const ProductDetail = props => {

    useEffect(() => {
        props.initData()

        return () => { 
            props.clearReducerState()
         }
    }, []);

    const ProductFormWithLoading = WithLoading(ProductForm)

    return (
        <ProductFormWithLoading {...props} isLoading={props.pageState instanceof LoadingState}/>
    );
};

const mapStateToProps = state => {
    return {
        pageState: state.productReducer.pageState
    }
}

const mapDispatchToProps = dispatch => {
    return {
        initData: () => dispatch(initData()),
        clearReducerState: () => dispatch(clearReducerState()),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductDetail));