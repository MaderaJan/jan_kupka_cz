import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux"
import Select from "react-select";
import { getSelectStyles, getInvalidFeedBackStyle, getFormControlStyles } from "../../../../../util/StylesHelper"

import strings from "../../../../../resources/strings";

import ImagePicker from "./ImagePicker/ImagePicker";

import { validateFormMulti, validateForm, createNewProduct, validateFromSelect, fetchProduct } from "../../../../../redux/action/ProductActions"
import { SuccessState, PrepareState } from "../../../../../model/data/states";
import PageNavigation from "../../../../../resources/pageNavigation";

const ProductDetail = props => {
    const [applyStyles, setApplyStyles] = useState(false);

    useEffect(() => {
        if (props.pageState instanceof PrepareState && props.id) {
            props.fetchProduct(props.id)
        } else if (props.pageState instanceof SuccessState) {
            props.history.push(PageNavigation.PRODUCT_LIST);
        }
    });

    const createNewProduct = () => {
        if (props.isFormValid) {
            props.createNewProduct(props.formControls, props.images, props.isInEditMode);
        } else {
            setApplyStyles(true);
        }
    };

    const formHandler = (event) => {
        const name = event.target.name;
        let value = event.target.value;

        props.validateForm(name, value, props.formControls)
    };

    const formHandleSelect = (selectedOption, action) => {
        props.validateFromSelect(action.name, selectedOption.value, selectedOption.label, props.formControls)
    }

    const formHandleMultiSelect = (selectedOption, action) => {
        props.validateFormMulti(selectedOption, action, props.formControls)
    }

    return (
        <form className="container mt-4">
            <div className="mb-4 mt-4">
                <label className="h4">{strings.photos}</label>
                <hr className="m-0 mb-2 mt-1" />
                <ImagePicker />
            </div>

            {/* GENERAL INFO */}
            <label className="h4">{strings.general_info}</label>
            <hr className="m-0 mb-2 mt-1" />
            <div className="form-row">
                <div className="col">
                    <div className="form-group">
                        <label className="form-label">{strings.name}</label>
                        <input
                            value={props.formControls.name.value}
                            className={getFormControlStyles(props.formControls.name.isValid, applyStyles)}
                            name="name"
                            type="text"
                            onChange={formHandler}
                            placeholder={strings.name}
                        />
                        <div className="invalid-feedback">
                            {strings.error_fill_field}
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="form-group">
                        <label>{strings.type}</label>
                        <Select
                            name="type"
                            onChange={formHandleSelect}
                            styles={getSelectStyles(props.formControls.type.isValid, applyStyles)}
                            value={
                                {
                                    value: props.formControls.type.key,
                                    label: props.formControls.type.value
                                }
                            }
                            options={props.productTypes.map(type => {
                                return {
                                    value: type._id,
                                    label: type.name
                                }
                            })} />
                        <div className={getInvalidFeedBackStyle(props.formControls.type.isValid, applyStyles)}>
                            {strings.error_choose_one}
                        </div>
                    </div>
                </div>
            </div>

            {/* PRODUCT DESCRIPTION */}
            <div className="form-row">
                <div className="col form-group">
                    <label className="form-label">{strings.product_description}</label>
                    <textarea
                        value={props.formControls.description.value}
                        onChange={formHandler}
                        name="description"
                        className={getFormControlStyles(props.formControls.description.isValid, applyStyles)}
                        rows="4"
                    />
                    <div className="invalid-feedback">
                        {strings.error_fill_field}
                    </div>
                </div>
            </div>

            {/* LEATHER */}
            <label className="h4 mt-4">{strings.leather}</label>
            <hr className="m-0 mb-2" />
            <div className="form-group">
                <label>{strings.kinds}</label>

                <Select
                    className="mb-2"
                    name="selectedLeathers"
                    isMulti
                    onChange={formHandleMultiSelect}
                    styles={getSelectStyles(props.formControls.selectedLeathers.isValid, applyStyles)}
                    value={props.formControls.selectedLeathers.value.map((leather, index) => {
                        return {
                            value: props.formControls.selectedLeathers.keys[index],
                            label: leather
                        }
                    })}
                    options={props.leathers.map(leather => {
                        return {
                            value: leather._id,
                            label: leather.name
                        }
                    })} />
                <div className={getInvalidFeedBackStyle(props.formControls.selectedLeathers.isValid, applyStyles)}>
                    {strings.error_choose_one}
                </div>

                <div className="form-group mb-4">
                    <label>{strings.consumption_dm2}</label>
                    <input
                        value={props.formControls.leatherConsumption.value}
                        className={getFormControlStyles(props.formControls.leatherConsumption.isValid, applyStyles)}
                        name="leatherConsumption"
                        type="number"
                        onChange={formHandler}
                        placeholder={strings.consumption_dm2}
                    />
                    <div className="invalid-feedback">
                        {strings.error_fill_field}
                    </div>
                </div>
            </div>

            {/* THREAD */}
            <label className="h4 mt-4">{strings.thread}</label>
            <hr className="m-0 mb-2 mt-1" />
            <div className="form-group">
                <label>{strings.kinds}</label>

                <Select
                    className="mb-2"
                    name="selectedThreads"
                    isMulti
                    onChange={formHandleMultiSelect}
                    styles={getSelectStyles(props.formControls.selectedThreads.isValid, applyStyles)}
                    value={props.formControls.selectedThreads.value.map((thread, index) => {
                        return {
                            value: props.formControls.selectedThreads.keys[index],
                            label: thread
                        }
                    })}
                    options={props.threads.map(thread => {
                        return {
                            value: thread._id,
                            label: thread.name
                        }
                    })} />
                <div className={getInvalidFeedBackStyle(props.formControls.selectedThreads.isValid, applyStyles)}>
                    {strings.error_choose_one}
                </div>
            </div>
            <div className="form-group mb-4">
                <label>{strings.consumption_m}</label>
                <input
                    value={props.formControls.threadConsumption.value}
                    className={getFormControlStyles(props.formControls.threadConsumption.isValid, applyStyles)}
                    name="threadConsumption"
                    type="number"
                    min="0"
                    onChange={formHandler}
                    placeholder={strings.consumption_m}
                />
                <div className="invalid-feedback">
                    {strings.error_fill_field}
                </div>
            </div>

            {/* COLOR */}
            <label className="h4  mt-4">{strings.color}</label>
            <hr className="m-0 mb-2 mt-1" />
            <div className="form-group">
                <label>{strings.kinds}</label>
                <Select
                    className="mb-2"
                    name="selectedColors"
                    isMulti
                    onChange={formHandleMultiSelect}
                    styles={getSelectStyles(props.formControls.selectedColors.isValid, applyStyles)}
                    value={props.formControls.selectedColors.value.map((color, index) => {
                        return {
                            value: props.formControls.selectedColors.keys[index],
                            label: color
                        }
                    })}
                    options={props.colors.map(color => {
                        return {
                            value: color._id,
                            label: color.name
                        }
                    })} />
                <div className={getInvalidFeedBackStyle(props.formControls.selectedColors.isValid, applyStyles)}>
                    {strings.error_choose_one}
                </div>
            </div>
            <div className="form-group mb-4">
                <label>{strings.color_ml}</label>
                <input
                    value={props.formControls.colorConsumption.value}
                    className={getFormControlStyles(props.formControls.colorConsumption.isValid, applyStyles)}
                    name="colorConsumption"
                    type="number"
                    min="0"
                    onChange={formHandler}
                    placeholder={strings.color_ml}
                />
                <div className="invalid-feedback">
                    {strings.error_fill_field}
                </div>
            </div>

            {/* PRICE INFO */}
            <label className="h4  mt-4">{strings.price_info}</label>
            <hr className="m-0 mb-2 mt-1" />
            <div className="form-row mb-2">
                <div className="col">
                    <div className="form-group">
                        <label className="text-truncate">
                            {strings.avg_work_time}
                        </label>
                        <input
                            value={props.formControls.avgWorkTime.value}
                            className={getFormControlStyles(props.formControls.avgWorkTime.isValid, applyStyles)}
                            name="avgWorkTime"
                            type="number"
                            min="0"
                            step="0.01"
                            onChange={formHandler}
                            placeholder={strings.avg_work_time}
                        />
                        <div className="invalid-feedback">
                            {strings.error_fill_field}
                        </div>
                    </div>
                </div>

                <div className="col">
                    <div className="form-group">
                        <label className="text-truncate">
                            {strings.clue_and_more}
                        </label>
                        <input
                            value={props.formControls.tools.value}
                            className={getFormControlStyles(props.formControls.tools.isValid, applyStyles)}
                            name="tools"
                            type="number"
                            min="0"
                            onChange={formHandler}
                            placeholder={strings.clue_and_more}
                        />
                        <div className="invalid-feedback">
                            {strings.error_fill_field}
                        </div>
                    </div>
                </div>
            </div>
            <div className="form-row">
                <div className="col-6">
                    <div className="form-group">
                        <label>{strings.tool_ware_marge}</label>
                        <input
                            value={props.formControls.toolsWareMarge.value}
                            className={getFormControlStyles(props.formControls.toolsWareMarge.isValid, applyStyles)}
                            name="toolsWareMarge"
                            type="number"
                            min="1"
                            onChange={formHandler}
                            placeholder={strings.tool_ware_marge}
                        />
                        <div className="invalid-feedback">
                            {strings.error_fill_field}
                        </div>
                    </div>
                </div>

                <div className="col">
                    <div className="form-group">
                    <label>{strings.min_price}</label>
                        <input
                            className="form-control"
                            type="text"
                            value={props.basePrice}
                            readOnly="readonly"
                            name="baseCost"
                        />
                    </div>
                </div>

                <div className="col">
                    <div className="form-group">
                    <label>{strings.sell_price}</label>
                        <input
                            className={getFormControlStyles(props.formControls.sellPrice.isValid, applyStyles)}
                            type="text"
                            value={props.formControls.sellPrice.value}
                            name="sellPrice"
                            type="number"
                            onChange={formHandler}
                            placeholder={strings.sell_price}
                        />
                        <div className="invalid-feedback">
                            {strings.error_fill_field}
                        </div>
                    </div>
                </div>
            </div>

            <hr className="m-0 mb-2 mt-1" />
            <div className="form-row">
                <div className="col-2 mt-2">
                    <button className="btn btn-primary btn-block p-2" type="button" onClick={createNewProduct}>
                        {props.isInEditMode ? strings.edit : strings.create}
                    </button>
                </div>
            </div>
            <div />
        </form>
    );
};

const mapStateToProps = state => {
    return {
        productTypes: state.productReducer.productTypes,
        colors: state.productReducer.colors,
        leathers: state.productReducer.leathers,
        threads: state.productReducer.threads,
        formControls: state.productReducer.formControls,
        basePrice: state.productReducer.basePrice,
        isFormValid: state.productReducer.isFormValid,
        pageState: state.productReducer.pageState,
        images: state.productReducer.images,
        isInEditMode: state.productReducer.isInEditMode
    }
}

const mapDispatchToProps = dispatch => {
    return {
        validateForm: (fieldName, value, formControls) => dispatch(validateForm(fieldName, value, formControls)),
        createNewProduct: (formControls, images, isInEditMode) => dispatch(createNewProduct(formControls, images, isInEditMode)),
        validateFromSelect: (name, key, value, formControls) => dispatch(validateFromSelect(name, key, value, formControls)),
        validateFormMulti: (selectedOption, action, formControls) => dispatch(validateFormMulti(selectedOption, action, formControls)),
        fetchProduct: (id) => dispatch(fetchProduct(id)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductDetail));