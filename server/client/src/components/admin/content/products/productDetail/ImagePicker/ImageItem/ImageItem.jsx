import React, { useState } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

import strings from "../../../../../../../resources/strings";

import "./ImageItem.css"

const ImageItem = props => {

    const renderContent = () => {
        if (props.image) {
            return <img className="image-item" src={props.image} />
        } else {
            return <FontAwesomeIcon
                className="mx-auto align-self-center"
                color="#000"
                icon={faPlus} />
        }
    }

    const renderDelete = () => {
        if (props.image) {
            return <span onClick={props.onDelete} className="mt-1 mx-auto align-self-center action-text">{strings.remove}</span>
        } 
    };

    const handleAddClick = () => {
        if (!props.image) {
            props.onAdd()
        } else {
            props.onMainChanged();
        }
    };

    return (
        <div className="d-flex m-2 flex-column">

            <div onClick={handleAddClick} className={"d-flex p-8 image-item-wrapper " + (props.isMain ? "image-main" : "border")}>
                {renderContent()}
            </div>
            {renderDelete()}
        </div>
    );
}

export default ImageItem;