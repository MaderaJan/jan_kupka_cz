import React, { useState } from "react";
import ImageItem from "./ImageItem/ImageItem";
import { addImage, removeImage, markImageAsMain } from "../../../../../../redux/action/ProductActions"
import { connect } from "react-redux"

const ImagePicker = props => {
    const [fileSelect, setFileSelect] = useState();

    const handleAdd = () => {
        fileSelect.click();
    };

    const handleChange = files => {
        const reader = new FileReader();

        reader.onload = function (event) {
            const imageBase64 = event.target.result;
            addImage(imageBase64, files[0]);
        };

        reader.readAsDataURL(files[0]);
    };

    const handleMainChanged = id => {
        props.markImageAsMain(props.images, id)
    }

    const handleDelete = id => {
        props.removeImage(props.images, id)
    };

    const addImage = (imageBase64, file) => {
        props.addImage(props.images, imageBase64, file)
    }

    return (
        <div className="d-flex flex-wrap">
            {props.images.map(img => (
                < ImageItem
                    key={img.id}
                    image={img.imageData}
                    onAdd={handleAdd}
                    isMain={img.isMain}
                    onDelete={() => handleDelete(img.id)}
                    onMainChanged={() => handleMainChanged(img.id)} />
            ))}

            <ImageItem onAdd={handleAdd} onDelete={handleDelete} />
            <input
                className="d-none"
                accept=".png, .jpg"
                ref={input => setFileSelect(input)}
                type="file"
                onChange={(e) => handleChange(e.target.files)} />
        </div>
    );
}

const mapStateToProps = state => {
    return {
        images: state.productReducer.images
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addImage: (images, imageFile, imageBase64) => dispatch(addImage(images, imageFile, imageBase64)),
        removeImage: (images, id) => dispatch(removeImage(images, id)),
        markImageAsMain: (images, id) => dispatch(markImageAsMain(images, id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ImagePicker);