import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom'
import { connect } from "react-redux"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog, faBars } from "@fortawesome/free-solid-svg-icons";

import strings from '../../../resources/strings';
import { UserLoggedOutState } from '../../../model/data/states';
import { fetchData, logoutUser, clearReducerState } from "./../../../redux/action/AdminTopBarActions"

const AdminTopBar = props => {

    useEffect(() => {
        props.fetchData()

        return () => { 
            props.clearReducerState()
         }
    }, [])

    useEffect(() => {
        if (props.pageState instanceof UserLoggedOutState) {
            props.history.push("/administrationForm")
        }
    })

    const logout = () => {
        props.logoutUser()
    }

    return (
        <div className="row shadow-sm p-2 bg-white rounded">
            <div className="col-6">
                <button className="btn">
                    <FontAwesomeIcon
                        icon={faBars}
                        onClick={props.onToggleSideBar}
                        className="sidebar-nav-icon float-right" />
                </button>
            </div>

            <div className="col-6 d-flex flex-row-reverse">
                <span className="mr-4">{`${strings.user}: ${props.userName}`}</span>

                <div className="mr-4 dropdown d-inline">
                    <a className="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <FontAwesomeIcon icon={faCog} />
                    </a>

                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a onClick={logout} className="dropdown-item" href="#">{strings.logout}</a>
                    </div>
                </div>
            </div>


        </div>
    );
}

const mapStateToProps = state => {
    return {
        pageState: state.adminTopBarReducer.pageState,
        userName: state.adminTopBarReducer.userName
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchData: () => dispatch(fetchData()),
        logoutUser: () => dispatch(logoutUser()),
        clearReducerState: () => dispatch(clearReducerState())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminTopBar));