import React, { useState } from "react";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux";

import AdminTopBar from "./topBar/AdminTopBar";
import SideBar from "./sideNavBar/SideBar";
import ContentWrapper from "../general/ContentWrapper/ContentWrapper";

import Products from "./content/products/Produtcs";
import ProductDetail from "./content/products/productDetail/ProductDetail";

import Warehouse from "./content/warehouse/Warehouse";
import WarehouseItem from "./content/warehouse/detail/WarehouseDetail";
import PageNavigation from "../../resources/pageNavigation";
import Dashboard from "./content/dashboard/Dashboard";

const Admin = () => {
    const [isSideBarShown, setSideBarShown] = useState(true);

    const toggleSideBar = () => {
        setSideBarShown(!isSideBarShown);
    };

    return (
        <React.Fragment>
            <SideBar
                onToggleSideBar={toggleSideBar}
                isSideBarShown={isSideBarShown}
            />

            <AdminTopBar
                onToggleSideBar={toggleSideBar}
                isSideBarShown={isSideBarShown}
            />

            <Switch>
                <Route
                    exact path={PageNavigation.ADMIN_DASHBOARD}
                    render={props => (
                        <ContentWrapper
                            isSideBarShown={isSideBarShown}
                            isAuthed={true}
                        >
                            <Dashboard />
                        </ContentWrapper>
                    )}
                />

                <Route
                    exact path={PageNavigation.PRODUCT_DETAIL_EDIT}
                    render={props => (
                        <ContentWrapper
                            isSideBarShown={isSideBarShown}
                            isAuthed={true}
                        >
                            <ProductDetail id={props.match.params.id} />
                        </ContentWrapper>
                    )}
                />
                <Route
                    exact path={PageNavigation.PRODUCT_DETAIL}
                    render={props => (
                        <ContentWrapper
                            isSideBarShown={isSideBarShown}
                            isAuthed={true}
                        >
                            <ProductDetail />
                        </ContentWrapper>
                    )}
                />
                <Route
                    exact path={PageNavigation.PRODUCT_LIST}
                    render={props => (
                        <ContentWrapper
                            isSideBarShown={isSideBarShown}
                            isAuthed={true}
                        >
                            <Products />
                        </ContentWrapper>
                    )}
                />

                <Route
                    exact path={PageNavigation.WAREHOUSE_ITEM_DETAIL_EDIT}
                    render={props => (
                        <ContentWrapper
                            isSideBarShown={isSideBarShown}
                            isAuthed={true}
                        >
                            <WarehouseItem id={props.match.params.id} />
                        </ContentWrapper>
                    )}
                />

                <Route
                    exact path={PageNavigation.WAREHOUSE_ITEM_DETAIL}
                    render={props => (
                        <ContentWrapper
                            isSideBarShown={isSideBarShown}
                            isAuthed={true}
                        >
                            <WarehouseItem />
                        </ContentWrapper>
                    )}
                />

                <Route
                    exact path={PageNavigation.WAREHOUSE_LIST}
                    render={props => (
                        <ContentWrapper
                            isSideBarShown={isSideBarShown}
                            isAuthed={true}
                        >
                            <Warehouse />
                        </ContentWrapper>
                    )}
                />
            </Switch>
        </React.Fragment>
    );
};

const mapStateToProps = (state) => {
    return {
        pageState: state.warehouseItemReducer.pageState
    }
}

export default connect(mapStateToProps, null)(Admin)