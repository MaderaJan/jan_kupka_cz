import React from "react";

import "./SideBar.css";

import strings from "../../../resources/strings";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faColumns, faWarehouse, faShoppingBag, faImages, faTimes } from "@fortawesome/free-solid-svg-icons";
import PageNavigation from "../../../resources/pageNavigation";

const SideBar = props => {
    return (
        <div
            id="wrapper" className={props.isSideBarShown ? "" : "toggled"}>
            <div id="sidebar-wrapper">
                <ul className="sidebar-nav">
                    <li className="sidebar-brand">
                        <a href="#" className="white">
                            Jan Kupka
                        </a>

                        <button className="btn float-right mt-3 mr-2">
                            <FontAwesomeIcon
                                icon={faTimes}
                                onClick={props.onToggleSideBar}
                                className="sidebar-nav-icon float-right" />
                        </button>
                    </li>
                    <li>
                        <span>
                            <FontAwesomeIcon
                                icon={faColumns}
                                className="sidebar-nav-icon"
                            />
                            <a href={PageNavigation.ADMIN_DASHBOARD}>{strings.dashboard}</a>
                        </span>
                    </li>

                    <li>
                        <FontAwesomeIcon
                            icon={faShoppingBag}
                            className="sidebar-nav-icon"
                        />
                        <a href={PageNavigation.PRODUCT_LIST}>{strings.products}</a>
                    </li>

                    <li>
                        <FontAwesomeIcon
                            icon={faWarehouse}
                            className="sidebar-nav-icon"
                        />
                        <a href={PageNavigation.WAREHOUSE_LIST}>{strings.warehouse}</a>
                    </li>

                    <li>
                        <FontAwesomeIcon
                            icon={faImages}
                            className="sidebar-nav-icon"
                        />
                        <a href="#">{strings.gallery}</a>
                    </li>
                </ul>
            </div>
        </div>
    );
}

export default SideBar;
