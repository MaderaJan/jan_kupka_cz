import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';

import strings from '../../../resources/strings';

import './AdminFormApp.css'

import { connect } from "react-redux";
import { loginSuperUser, validateForm, clearReducerState } from '../../../redux/action/AdminFormActions';
import PageNavigation from '../../../resources/pageNavigation';

const AdminFormApp = (props) => {
    const styles = {
        inputStyle: "form-control mb-4 simple-input",
        buttonStyle: "btn btn-block btn-primary my-4 simple-input"
    };

    useEffect(() => {
        return () => { 
            props.clearReducerState()
         }
    }, []);

    useEffect(() => {
        if (props.isUserLogged) {
            props.history.push(PageNavigation.ADMIN_DASHBOARD)
        }
    });

    const changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        props.validateForm(name, value, props.formControls);
    }

    const onLoginClicked = () => {
        const superUser = {
            name: props.formControls.name.value,
            password: props.formControls.password.value
        }

        props.loginSuperUser(superUser);
    }

    return (
        <div className="vertical-center">
            <form className="container text-center w-50">
                <label className="mb-4 h3">{strings.login}</label>

                <input
                    onChange={changeHandler}
                    isValid={props.formControls.name.isValid}
                    className={styles.inputStyle}
                    name="name"
                    type="text"
                    placeholder={strings.username} />

                <input
                    onChange={changeHandler}
                    isValid={props.formControls.password.isValid}
                    className={styles.inputStyle}
                    name="password"
                    type="password"
                    placeholder={strings.password} />

                <button
                    onClick={onLoginClicked}
                    className={styles.buttonStyle}
                    disabled={!props.isFormValid}
                    type="button" >
                    {strings.login_in}
                </button>
                {
                    props.formError &&
                    <div className="alert alert-danger simple-input" role="alert">{props.formError}</div>
                }
            </form>
        </div>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginSuperUser: superUser => dispatch(loginSuperUser(superUser)),
        validateForm: (name, value, formControls) => dispatch(validateForm(name, value, formControls)),
        clearReducerState: () => dispatch(clearReducerState())
    }
}

const mapStateToProps = (state) => {
    return {
        formError: state.adminFormReducer.formError,
        isUserLogged: state.adminFormReducer.isUserLogged,
        isFormValid: state.adminFormReducer.isFormValid,
        formControls: state.adminFormReducer.formControls
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminFormApp))