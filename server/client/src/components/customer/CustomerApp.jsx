import React from 'react';
import NavBar from './NavBar';
import { Switch, Route } from "react-router-dom";

import PageNavigation from "../../resources/pageNavigation"

import Home from './home/Home.jsx';
import Shop from './shop/Shop.jsx';
import Contact from './contact/Contact';
import NotFound404 from '../general/NotFound404'

const CustomerApp = () => {
    return (
        <React.Fragment>
            <NavBar />
            <Switch>
                <Route path={PageNavigation.CONTACT} component={Contact} />
                <Route excat path={PageNavigation.SHOP} component={Shop} />
                <Route exact path={PageNavigation.HOME} component={Home} />
                <Route component={NotFound404} />
            </Switch>
        </React.Fragment>
    )
}

export default CustomerApp;