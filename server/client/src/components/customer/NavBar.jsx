import React from 'react';

const NavBar = () => {
    return ( 
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand mb-0 h1" href="/">Jan Kupka</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarToggler">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <a className="nav-link" href="/">Domů</a>
                    </li>

                    <li className="nav-item">
                        <a className="nav-link" href="shop">Zboží</a>
                    </li>

                    <li className="nav-item">
                        <a className="nav-link" href="contact">Kontakt</a>
                    </li>
                </ul>
            </div>
        </nav>
        );
}
 
export default NavBar;