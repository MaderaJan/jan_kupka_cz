import React from 'react';

const Home = () => {
    return (
        <div className="m-4">
            <h1>Home</h1>
            <span>Odkaz pro přihlášení do administarace <a href="http://jan-kupka.herokuapp.com/administrationForm">zde</a></span><br/>
            <span>Jméno: admin</span><br />
            <span>Heslo: heslo</span>
        </div>
    );
}

export default Home;