import { ValidationRule } from "./ValidationRule";

export interface TextInput {
    value: any,
    isValid: boolean
    validationRule: ValidationRule
}

export class FormTextInput implements TextInput {
    value: string = ""
    isValid: boolean = false
    validationRule: ValidationRule

    constructor(validationRule: ValidationRule, value?: string) {
        this.validationRule = validationRule
        if (value) {
            this.value = value
            this.isValid = true
        }
    }

    set(value: string): FormTextInput {
        this.value = value
        return this
    }
}

export class SelectInput implements TextInput {
    value: string = ""
    key: string = ""
    isValid: boolean = false
    validationRule: ValidationRule

    constructor(validationRule: ValidationRule, key?: string, value?: string) {
        this.validationRule = validationRule
        this.isValid = false

        if (value) {
            this.value = value
            this.isValid = true
        }
        if (key) this.key = key
    }

    set(value: string): FormTextInput {
        this.value = value
        return this
    }
}

export class MultiSelectInput implements TextInput {
    value: Array<string> = []
    keys: Array<string> = []
    isValid: boolean = false
    validationRule: ValidationRule

    constructor(validationRule: ValidationRule, keys?: Array<string>, value?: Array<string>) {
        this.validationRule = validationRule

        if (value) {
            this.value = value
            this.isValid = value.length > 0
        }
        
        if (keys) this.keys = keys
    }
}