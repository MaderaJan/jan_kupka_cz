export class ValidationRule {
    validate(value: any): boolean {
        throw new Error("Method 'say()' must be implemented.");
    }
}

export class MinLength extends ValidationRule {
    minLength: number

    constructor(minLength: number) {
        super();
        this.minLength = minLength
    }

    validate(value: string): boolean {
        return value.length >= this.minLength
    }
}

export class GreaterThan extends ValidationRule {
    greaterThen: number

    constructor(greaterThen: number) {
        super();
        this.greaterThen = greaterThen
    }

    validate(value: number): boolean {
        if (value as number != undefined) {
            return value > this.greaterThen
        } else {
            throw new Error("Method wrong type provided");
        }
    }
}

export class MinSelectedItems extends ValidationRule {
    minSelectedItems: number

    constructor(minSelectedItems: number) {
        super();
        this.minSelectedItems = minSelectedItems
    }

    validate(value: Array<string>): boolean {
        if (value as Array<any> != undefined) {
            return value.length >= this.minSelectedItems
        } else {
            throw new Error("Method wrong type provided");
        }
    }
}