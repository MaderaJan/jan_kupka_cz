import { TextInput } from "./FormTextInput";

export interface FormControls {
    [key: string]: TextInput
}