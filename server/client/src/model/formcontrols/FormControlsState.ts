import { FormControls } from "./FormControls";

export interface FormControlState {
    isFormValid: boolean,
	formControls: FormControls
}