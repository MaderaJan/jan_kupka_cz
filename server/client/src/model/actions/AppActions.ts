import { Actions } from "./Actions";

export interface AppActions {
    type: Actions,
    payload: any
}