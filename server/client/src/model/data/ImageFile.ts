export class ImageFile {
    id: number
    imageData: string
    imageFile?: File
    isMain: boolean

    constructor(id: number, imageData: string, isMain: boolean, imageFile?: File) {
        this.id = id
        this.imageData = imageData
        this.imageFile = imageFile
        this.isMain = isMain
    }
}