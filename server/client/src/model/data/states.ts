export abstract class PageState { }

export class LoadingState extends PageState { }
export class PrepareState extends PageState { }
export class ReadyState extends PageState { }
export class ModifiedState extends PageState {}
export class SuccessState extends PageState { }
export class UserLoggedOutState extends PageState { }

export interface BaseReducerState {
    pageState: PageState
}