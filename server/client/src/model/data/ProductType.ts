export interface ProductType {
    _id: string,
    type: string,
    name?: string
}