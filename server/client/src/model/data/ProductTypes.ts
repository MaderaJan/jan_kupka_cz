export interface BaseProductType {
    code: string,
    cost: number
}

export interface Leather extends BaseProductType {}

export interface Color extends BaseProductType {}

export interface Thread extends BaseProductType {}