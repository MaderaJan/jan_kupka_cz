export interface WarehouseItemType {
    _id: string,
    code: string,
    name?: string
}