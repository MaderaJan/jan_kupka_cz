import { ProductType } from "./ProductType";
import { WarehouseItem } from "./WarehouseItem";

export interface Product {
    _id: string,
    name: string,
    description: string,
    type: ProductType,
    avgWorkTime: number,
    tools: number,
    colorConsumption: number,
    toolsWareMarge: number,
    sellPrice: number,
    leatherConsumption: number,
    threadConsumption: number,
    basePrice: number,
    leathers: Array<string>,
    threads: Array<string>,
    colors: Array<string>,
    images: Array<string>,  
    logo?: string,
    status?: string  
}

export interface StatusDto {
    id: string,
    status: string
}

export interface ChangeStatusesRequest {
    statuses: Array<StatusDto>
}

export interface ProductResponse {
    _id: string,
    name: string,
    description: string,
    type: ProductType,
    avgWorkTime: number,
    tools: number,
    colorConsumption: number,
    toolsWareMarge: number,
    sellPrice: number,
    leatherConsumption: number,
    threadConsumption: number,
    basePrice: number,
    leathers: Array<WarehouseItem>,
    threads: Array<WarehouseItem>,
    colors: Array<WarehouseItem>,
    images: Array<string>,    
}