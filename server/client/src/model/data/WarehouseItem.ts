import { WarehouseItemType } from "./WarehouseItemType";
import { FormControls } from "../formcontrols/FormControls";

export interface WarehouseItem {
    _id: string,
    name: string,
    cost: number,
    amount: number,
    type: WarehouseItemType,
    unit: string
}