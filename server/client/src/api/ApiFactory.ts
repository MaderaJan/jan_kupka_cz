import AuthApi from "./AuthApi";
import BaseService from "./BaseService";
import DashboardApi from "./DashboardApi";
import ProductApi from "./ProductApi";
import WarehouseApi from "./WarehouseApi";

class ApiFactory {

    warehouseApi: WarehouseApi = new WarehouseApi()
    productApi: ProductApi = new ProductApi()
    authApi: AuthApi = new AuthApi()
    dashboardApi: DashboardApi = new DashboardApi()

    get(identificator: string): BaseService {
        switch (identificator) {
            case WarehouseApi.name:
                return this.warehouseApi

            case ProductApi.name:
                return this.productApi

            case AuthApi.name:
                return this.authApi

            case DashboardApi.name:
                return this.dashboardApi

            default:
                throw Error("Unknown service")
        }
    }
}

export default ApiFactory