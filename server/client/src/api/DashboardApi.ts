import axios from "axios";
import UserRepository from "../repository/UserRepository";
import { endPoints } from "../resources/kupkaApi";

class DashboardApi {

    userRepository: UserRepository = new UserRepository()

    getData = () => axios.get(endPoints.dashboard, this.userRepository.createHeaders())
};

export default DashboardApi