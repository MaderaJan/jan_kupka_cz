import axios from "axios";
import { ChangeStatusesRequest } from "../model/data/Product";
import UserRepository from "../repository/UserRepository";
import { endPoints } from "../resources/kupkaApi";

class ProductApi {

    userRepository: UserRepository = new UserRepository()

    getAllProducts = () => axios.get(endPoints.products, this.userRepository.createHeaders())

    getProductById = (id: string) => axios.get(endPoints.product, this.userRepository.createHeadersWithParams({ id: id }))

    deleteProdutById = (id: string) => axios.delete(endPoints.product, this.userRepository.createHeadersWithParams({ id: id }))

    getProductTypes = () => axios.get(endPoints.product_types, this.userRepository.createHeaders())

    createProduct = (formData: FormData) =>
        axios.post(endPoints.product, formData, this.userRepository.createHeadersWithMultiPart())

    updateProduct = (formData: FormData) =>
        axios.put(endPoints.product, formData, this.userRepository.createHeadersWithMultiPart())

    postUpdateStatuses = (request: ChangeStatusesRequest) =>
        axios.post(endPoints.products_list_status_update, request, this.userRepository.createHeaders())
};

export default ProductApi