import axios from "axios";
import { SuperUser } from "../model/data/SuperUser"
import { endPoints } from "../resources/kupkaApi"

class AuthApi {

    loginAdmin = (superUser: SuperUser) => axios.post(endPoints.login, superUser)

};

export default AuthApi