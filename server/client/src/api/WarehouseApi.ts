import axios from "axios";
import { endPoints } from "../resources/kupkaApi";
import { WarehouseItemRequest } from "../model/request/WareHouseItemRequest";
import UserRepository from "../repository/UserRepository";
import BaseService from "./BaseService";

class WarehouseApi implements BaseService {

    userRepository: UserRepository = new UserRepository()

    getAllWarehouseItems = () => axios.get(endPoints.warehouse_items, this.userRepository.createHeaders())

    getAllWarehouseItemTypes = () => axios.get(endPoints.warehouse_item_types, this.userRepository.createHeaders())

    createWarehouseItem = (item: WarehouseItemRequest) => axios.post(endPoints.warehouse_item, { item: item }, this.userRepository.createHeaders())

    updateWarehouseItem = (item: WarehouseItemRequest) => axios.put(endPoints.warehouse_item, { item: item }, this.userRepository.createHeaders())

    getWarehouseItemById = (id: string) => axios.get(endPoints.warehouse_item, this.userRepository.createHeadersWithParams({ id: id }))

    deleteWarehouseItem = (id: string) => axios.delete(endPoints.warehouse_item, this.userRepository.createHeadersWithParams({ id: id }))
};

export default WarehouseApi