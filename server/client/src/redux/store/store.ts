import { createStore, applyMiddleware, compose, combineReducers } from "redux";

import AdminFormReducer from "../reducer/AdminFormReducer";
import WarehouseReducer from "../reducer/WarehouserReducer";
import WarehouseItemReducer from "../reducer/WarehousDetailReducer";

import thunk, { ThunkMiddleware } from "redux-thunk"
import { AppActions } from "../../model/actions/AppActions";
import ProductListReducer from "../reducer/ProducListsReducer";
import ProductReducer from "../reducer/ProductReducer";
import ApiFactory from "../../api/ApiFactory";
import AdminTopBarReducer from "../reducer/AdminTopBarReducer";
import dashboardReducer from "../reducer/DashboardReducer";

const rootReducer = combineReducers({
  adminFormReducer: AdminFormReducer, 
  warehouseReducer: WarehouseReducer,
  warehouseItemReducer: WarehouseItemReducer,
  productListReducer: ProductListReducer,
  productReducer: ProductReducer,
  adminTopBarReducer: AdminTopBarReducer,
  dashboardReducer: dashboardReducer
})

export type AppState = ReturnType<typeof rootReducer>;

const apiFactory = new ApiFactory()

const store = createStore(
  rootReducer,
  applyMiddleware(thunk.withExtraArgument(apiFactory) as ThunkMiddleware<AppState, AppActions>)
);

store.subscribe(() => console.log(store.getState()))

export default store;