import { Product, StatusDto } from "../../model/data/Product";
import { AppActions } from "../../model/actions/AppActions";
import { Actions } from "../../model/actions/Actions";
import { BaseReducerState, LoadingState, ModifiedState, ReadyState } from "../../model/data/states";


interface ProductListState extends BaseReducerState {
    items: Array<Product>
    changedStatuses: Array<StatusDto>
    isLoading: boolean
}

const initState: ProductListState = {
    items: [],
    changedStatuses: [],
    isLoading: false,
    pageState: new LoadingState()
}

const productListReducer = (state = initState, action: AppActions) => {
    switch (action.type) {

        case Actions.PRODUCT_LIST_FETCHED:
            return {
                ...state,
                items: action.payload,
                pageState: new ReadyState()
            }

        case Actions.PRODUCT_LIST_ITEM_DELETED:
            return {
                ...state,
                items: state.items.filter(item => {
                    return item._id != action.payload
                })
            }

        case Actions.PRODUCT_STATUS_CHANGED:
            return {
                ...state,
                items: action.payload.products,
                changedStatuses: action.payload.statuses,
                pageState: new ModifiedState()
            }

        case Actions.PRODUCT_STATUS_UPDATED:
            return {
                ...state,
                changedStatuses: [],
                pageState: new ReadyState()
            }

        case Actions.LOADING_STATE:
            return {
                ...state,
                pageState: new LoadingState()
            }

        default:
            return state
    }
}

export default productListReducer