import { Actions } from "../../model/actions/Actions";
import { AppActions } from "../../model/actions/AppActions";
import { FormTextInput } from "../../model/formcontrols/FormTextInput";
import { MinLength } from "../../model/formcontrols/ValidationRule";
import { FormControlState } from "../../model/formcontrols/FormControlsState";

interface AdminFormState extends FormControlState {
	formError: string,
	isUserLogged: boolean,
	loading: boolean,
}

const initState: AdminFormState = {
	formError: "",
	isUserLogged: false,
	loading: false,
	isFormValid: false,
	formControls: { name: new FormTextInput(new MinLength(3)), password: new FormTextInput(new MinLength(3)) }
};

const adminReducer = (state = initState, action: AppActions) => {
	switch (action.type) {
		case Actions.SUPER_USER_LOGIN:
			return {
				...state,
				loading: true
			}

		case Actions.SUPER_USER_LOGIN_SUCCESS:
			return {
				...state,
				isUserLogged: true,
				loading: false
			};

		case Actions.SUPER_USER_LOGIN_FAILED:
			return {
				...state,
				isUserLogged: false,
				formError: action.payload,
				loading: false
			};

		case Actions.VALIDATE_FORM:
			return {
				...state,
				isFormValid: action.payload.isFormValid,
				formControls: action.payload.formControls
			}

		case Actions.CLEAR_REDUCER_STATE:
			return initState

		default:
			return state;
	}
};

export default adminReducer;