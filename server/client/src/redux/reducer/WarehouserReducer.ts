import { AppActions } from "../../model/actions/AppActions";
import { Actions } from "../../model/actions/Actions";
import { WarehouseItem } from "../../model/data/WarehouseItem";
import { BaseReducerState, LoadingState, ReadyState } from "../../model/data/states";

interface WarehouseState extends BaseReducerState {
    items: Array<WarehouseItem>,
}

const initState: WarehouseState = {
    items: [],
    pageState: new LoadingState()
};

const warehouseReducer = (state = initState, action: AppActions) => {
    switch (action.type) {
        case Actions.FETCH_WAREHOUSE_ITEMS:
            return {
                ...state,
                items: action.payload,
                pageState: new ReadyState()
            };

        case Actions.DELETE_WAREHOUSE_ITEM:
            return {
                ...state,
                pageState: new ReadyState(),
                items: state.items.filter((value, index, arr) => {
                    return value._id != action.payload;
                })
            }

        default:
            return state
    };
};

export default warehouseReducer;