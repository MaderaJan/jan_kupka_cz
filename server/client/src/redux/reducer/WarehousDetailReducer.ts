import { AppActions } from "../../model/actions/AppActions"
import { FormControlState } from "../../model/formcontrols/FormControlsState"
import { FormTextInput, SelectInput } from "../../model/formcontrols/FormTextInput"
import { MinLength, GreaterThan } from "../../model/formcontrols/ValidationRule"
import { WarehouseItemType } from "../../model/data/WarehouseItemType"
import { Actions } from "../../model/actions/Actions"
import { BaseReducerState, LoadingState, ReadyState, SuccessState, PrepareState } from "../../model/data/states"

interface WarehouseItemState extends FormControlState, BaseReducerState {
    types: Array<WarehouseItemType>
    id?: string,
    isInEditMode: boolean
}

const initState: WarehouseItemState = {
    id: undefined,
    types: [],
    isFormValid: false,
    isInEditMode: false,
    pageState: new LoadingState(),
    formControls: {
        name: new FormTextInput(new MinLength(3)),
        cost: new FormTextInput(new GreaterThan(0)),
        amount: new FormTextInput(new GreaterThan(1)),
        unit: new SelectInput(new MinLength(1)),
        type: new SelectInput(new MinLength(1)),
    }
}

const warehouseItemReducer = (state = initState, action: AppActions) => {
    switch (action.type) {

        case Actions.WAREHOUSE_ITEMS_FETCHED:
            return {
                ...state,
                pageState: new PrepareState(),
                types: action.payload
            }

        case Actions.WAREHOUSE_ITEM_FETCHED:
            return {
                ...state,
                pageState: new ReadyState(),
                id: action.payload.id,
                formControls: action.payload.formControls,
                isFormValid: action.payload.isFormValid,
                isInEditMode: true
            }

        case Actions.VALIDATE_FORM:
            return {
                ...state,
                isFormValid: action.payload.isFormValid,
                formControls: action.payload.formControls
            }

        case Actions.WAREHOUSE_ITEM_SAVED:
            return {
                ...state,
                pageState: new SuccessState()
            }

        case Actions.CLEAR_REDUCER_STATE:
            return initState

        default:
            return state
    }
}

export default warehouseItemReducer