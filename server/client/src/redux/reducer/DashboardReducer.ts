import { Actions } from "../../model/actions/Actions";
import { AppActions } from "../../model/actions/AppActions";
import DashboardInfo from "../../model/data/DashboardInfo";
import { BaseReducerState, LoadingState, ReadyState } from "../../model/data/states";

interface DashboardState extends BaseReducerState {
    dashboardInfo: DashboardInfo
}

const initState: DashboardState = {
    pageState: new ReadyState(),
    dashboardInfo: {
        productOnline: 0,
        productOffline: 0,
        warehouseItemsCounts: 0
    }
}

const dashboardReducer = (state = initState, action: AppActions) => {
    switch (action.type) {
        case Actions.LOADING_STATE:
            return {
                ...state,
                pageState: new LoadingState()
            }

            case Actions.DASHBOARD_DATA_FECHED:
                return {
                    ...state,
                    pageState: new ReadyState(),
                    dashboardInfo: action.payload
                }

        default:
            return state
    }
}

export default dashboardReducer