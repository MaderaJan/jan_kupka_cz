import { Actions } from "../../model/actions/Actions"
import { AppActions } from "../../model/actions/AppActions"
import { BaseReducerState, LoadingState, PageState, PrepareState, ReadyState, UserLoggedOutState } from "../../model/data/states"

interface AdminTopBarState extends BaseReducerState {
    userName: string
}

const initState: AdminTopBarState = {
    userName: "",
    pageState: new PrepareState()
}

const adminTopBarReducer = (state = initState, action: AppActions) => {
    switch (action.type) {

        case Actions.ADMIN_TOP_BAR_DATA_FECHTED:
            return {
                ...state,
                userName: action.payload
            }

        case Actions.ADMIN_TOP_BAR_USER_LOGGED_OUT:
            return {
                ...state,
                pageState: new UserLoggedOutState() 
            }

        case Actions.CLEAR_REDUCER_STATE:
            return {
                initState
            }

        default:
            return state
    }
}

export default adminTopBarReducer