import { ProductType } from "../../model/data/ProductType";
import { Color, Thread, Leather } from "../../model/data/ProductTypes";
import { AppActions } from "../../model/actions/AppActions";
import { Actions } from "../../model/actions/Actions";
import { FormTextInput, MultiSelectInput, SelectInput } from "../../model/formcontrols/FormTextInput";
import { MinLength, MinSelectedItems, GreaterThan } from "../../model/formcontrols/ValidationRule";
import { FormControlState } from "../../model/formcontrols/FormControlsState";
import strings from "../../resources/strings";
import { PageState, ReadyState, LoadingState, SuccessState, PrepareState } from "../../model/data/states";
import { ImageFile } from "../../model/data/ImageFile";

interface ProductState extends FormControlState {
    productTypes: Array<ProductType>,
    leathers: Array<Leather>,
    threads: Array<Thread>,
    colors: Array<Color>,
    basePrice: string,
    pageState: PageState,
    images: Array<ImageFile>,
    isInEditMode: boolean,
    id?: string
}

const initState: ProductState = {
    productTypes: [],
    leathers: [],
    threads: [],
    colors: [],
    isFormValid: false,
    basePrice: strings.no_price,
    pageState: new LoadingState(),
    images: [],
    isInEditMode: false,
    id: undefined,
    formControls: {
        name: new FormTextInput(new MinLength(3)),
        description: new FormTextInput(new MinLength(3)),
        type: new SelectInput(new MinLength(1)),
        avgWorkTime: new FormTextInput(new GreaterThan(0)),
        tools: new FormTextInput(new GreaterThan(0)),
        colorConsumption: new FormTextInput(new GreaterThan(0)),
        toolsWareMarge: new FormTextInput(new GreaterThan(0)),
        sellPrice: new FormTextInput(new GreaterThan(0)),
        leatherConsumption: new FormTextInput(new GreaterThan(0)),
        threadConsumption: new FormTextInput(new GreaterThan(0)),
        selectedLeathers: new MultiSelectInput(new MinSelectedItems(1)),
        selectedThreads: new MultiSelectInput(new MinSelectedItems(1)),
        selectedColors: new MultiSelectInput(new MinSelectedItems(1)),
    },
}

const productReducer = (state = initState, action: AppActions) => {
    switch (action.type) {

        case Actions.PRODUCT_INIT_DATA_FETCHED:
            return {
                ...state,
                productTypes: action.payload.types,
                leathers: action.payload.leathers,
                threads: action.payload.threads,
                colors: action.payload.colors,
                pageState: new PrepareState()
            }

        case Actions.PRODUCT_FETCHED:
            return {
                ...state,
                id: action.payload.id,
                formControls: action.payload.formControls,
                isFormValid: action.payload.isFormValid,
                basePrice: action.payload.basePrice,
                images: action.payload.images,
                isInEditMode: true,
                pageState: new ReadyState()
            }

        case Actions.VALIDATE_FORM:
            return {
                ...state,
                isFormValid: action.payload.isFormValid,
                formControls: action.payload.formControls,
                basePrice: action.payload.basePrice
            }

        case Actions.PRODUCT_CREATED:
            return {
                ...state,
                pageState: new SuccessState()
            }

        case Actions.PRODUCT_IMAGE_CHANGED:
            return {
                ...state,
                images: action.payload
            }

        case Actions.CLEAR_REDUCER_STATE:
            return initState

        default:
            return state
    }
}

export default productReducer