import strings from "../../resources/strings"
import { authToken, loggedAdminUser } from "../../resources/constants";

import { Dispatch } from "redux";
import { Actions } from "../../model/actions/Actions";
import { AppActions } from "../../model/actions/AppActions";
import { SuperUser } from "../../model/data/SuperUser";
import { updateFormControls, validateFormControls } from "../../util/FormValidator";
import { FormControls } from "../../model/formcontrols/FormControls";
import AuthApi from "../../api/AuthApi";
import ApiFactory from "../../api/ApiFactory";

export const loginSuperUser = (superUser: SuperUser) => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        dispatch(userLogin())

        const authApi = apiFactory.get(AuthApi.name) as AuthApi
        return authApi.loginAdmin(superUser)
            .then(res => {
                if (res.status == 200) {
                    localStorage.setItem(authToken, res.data.token);
                    localStorage.setItem(loggedAdminUser, res.data.user);

                    dispatch(userLoginSuccess());
                }
            })
            .catch(err => {
                if (err.response.status == 401) {
                    dispatch(userLoginError(strings.error_wrong_login))
                } else {
                    dispatch(userLoginError(strings.error_unknown))
                }
            });
    };
};

const userLogin = (): AppActions => {
    return {
        type: Actions.SUPER_USER_LOGIN,
        payload: null
    }
}

const userLoginSuccess = (): AppActions => {
    return {
        type: Actions.SUPER_USER_LOGIN_SUCCESS,
        payload: null
    }
}

const userLoginError = (error: string): AppActions => {
    return {
        type: Actions.SUPER_USER_LOGIN_FAILED,
        payload: error
    }
}

export const validateForm = (name: string, value: string, formControls: FormControls) => {
    return (dispatch: Dispatch<AppActions>) => {
        const updatedFormControls: FormControls = updateFormControls(name, value, formControls)
        const isFormValid = validateFormControls(updatedFormControls)

        dispatch(formValidation(isFormValid, updatedFormControls))
    }
}

const formValidation = (isFormValid: boolean, formControls: FormControls): AppActions => {
    return {
        type: Actions.VALIDATE_FORM,
        payload: {
            isFormValid: isFormValid,
            formControls: formControls
        }
    }
}

export const clearReducerState = (): AppActions => {
    return {
        type: Actions.CLEAR_REDUCER_STATE,
        payload: {}
    }
}