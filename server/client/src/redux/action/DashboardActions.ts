import { Dispatch } from "redux"
import ApiFactory from "../../api/ApiFactory"
import DashboardApi from "../../api/DashboardApi"
import { Actions } from "../../model/actions/Actions"
import { AppActions } from "../../model/actions/AppActions"
import DashboardInfo from "../../model/data/DashboardInfo"

export const fetchData = () => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        dispatch(loading())

        const dashboardApi = apiFactory.get(DashboardApi.name) as DashboardApi
        dashboardApi.getData()
            .then(res => {
                const dashboardInfo: DashboardInfo = res.data
                dispatch(dashboardDataFetched(dashboardInfo))
            }).catch(err => console.error)
    }
}

const dashboardDataFetched = (dashboardInfo: DashboardInfo): AppActions => {
    return {
        type: Actions.DASHBOARD_DATA_FECHED,
        payload: dashboardInfo
    }
}

const loading = (): AppActions => {
    return {
        type: Actions.LOADING_STATE,
        payload: {}
    }
} 