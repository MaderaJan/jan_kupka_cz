import { Actions } from "../../model/actions/Actions";
import { Dispatch } from "redux";
import { AppActions } from "../../model/actions/AppActions";
import { WarehouseItem } from "../../model/data/WarehouseItem";
import WarehouseApi from "../../api/WarehouseApi";
import ApiFactory from "../../api/ApiFactory";

export const fetchWarehouseItems = () => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        const warehouseApi = apiFactory.get(WarehouseApi.name) as WarehouseApi
        return warehouseApi.getAllWarehouseItems()
            .then(response => {
                console.log(response);
                dispatch(warehouseItems(response.data));
            })
            .catch(err => {
                console.log(err);
            });
    }
}

const warehouseItems = (items: Array<WarehouseItem>): AppActions => {
    return {
        type: Actions.FETCH_WAREHOUSE_ITEMS,
        payload: items
    }
}

export const deleteWarehouseItem = (id: string) => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        const warehouseApi = apiFactory.get(WarehouseApi.name) as WarehouseApi
    
        return warehouseApi.deleteWarehouseItem(id)
            .then(res => dispatch(itemDeleted(id)))
            .catch(err => console.log(err))
    }
}

const itemDeleted = (id: string): AppActions => {
    return {
        type: Actions.DELETE_WAREHOUSE_ITEM,
        payload: id
    }
}