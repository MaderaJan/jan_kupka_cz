import axios from "axios";
import { Dispatch } from "redux";

import strings from "../../resources/strings";
import { endPoints } from "../../resources/kupkaApi";

import { WarehouseItem } from "../../model/data/WarehouseItem";
import { WarehouseItemType } from "../../model/data/WarehouseItemType";
import { WarehouseItemRequest } from "../../model/request/WareHouseItemRequest";
import { Actions } from "../../model/actions/Actions";
import { AppActions } from "../../model/actions/AppActions";

import { FormControls } from "../../model/formcontrols/FormControls";
import { FormTextInput, SelectInput } from "../../model/formcontrols/FormTextInput";
import { MinLength, GreaterThan } from "../../model/formcontrols/ValidationRule";

import { updateFormControls, validateFormControls, updateFormControlsSelect } from "../../util/FormValidator";
import WarehouseApi from "../../api/WarehouseApi";
import ApiFactory from "../../api/ApiFactory";

export const fetchWareHouseItemTypes = () => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        const warehouseApi = apiFactory.get(WarehouseApi.name) as WarehouseApi
        return warehouseApi.getAllWarehouseItemTypes()
            .then(res => {
                const warehouseItems: Array<WarehouseItemType> = res.data.map((item: any) => {
                    const typesStrings = strings.item_type

                    const warehouseItemType: WarehouseItemType = {
                        _id: item._id,
                        code: item.code,
                        name: typesStrings.find(type => type.code === item.code)!.name
                    }

                    return warehouseItemType
                })

                dispatch(warehouseItemsFetched(warehouseItems))
            })
            .catch(err => console.log(err));
    }
}

const warehouseItemsFetched = (items: Array<WarehouseItemType>): AppActions => {
    return {
        type: Actions.WAREHOUSE_ITEMS_FETCHED,
        payload: items
    }
}

export const fetchWarehouseItem = (id: string) => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {

        const warehouseApi = apiFactory.get(WarehouseApi.name) as WarehouseApi
        return warehouseApi.getWarehouseItemById(id)
            .then(res => {
                const updatedFormControls = mapItemToFormControls(res.data)
                dispatch(setupWarehouseItem(updatedFormControls, id))
            })
            .catch(err => console.log(err))
    }
}

const mapItemToFormControls = (item: WarehouseItem): FormControls => {
    const type = strings.item_type.find(type => type.code === item.type.code)!

    return {
        name: new FormTextInput(new MinLength(3), item.name),
        cost: new FormTextInput(new GreaterThan(1), item.cost + ""),
        amount: new FormTextInput(new GreaterThan(1), item.amount + ""),
        unit: new SelectInput(new MinLength(1), item.unit, item.unit),
        type: new SelectInput(new MinLength(1), type.code, type.name),
    }
}

const setupWarehouseItem = (formControls: FormControls, id: string): AppActions => {
    return {
        type: Actions.WAREHOUSE_ITEM_FETCHED,
        payload: {
            id: id,
            formControls: formControls,
            isFormValid: true
        }
    }
}

export const createWarehouseItem = (formControls: FormControls, shouldModify: boolean) => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        const types: Array<WarehouseItemType> = getState().warehouseItemReducer.types
        const id = getState().warehouseItemReducer.id
        const item: WarehouseItemRequest = createRequest(formControls, types, id);

        const warehouseApi = apiFactory.get(WarehouseApi.name) as WarehouseApi
        const apiRequest = shouldModify ? warehouseApi.updateWarehouseItem(item) : warehouseApi.createWarehouseItem(item)

        return apiRequest
            .then(res => {
                dispatch(itemCreated())
            })
            .catch(err => console.log(err));
    }
}

const itemCreated = (): AppActions => {
    return {
        type: Actions.WAREHOUSE_ITEM_SAVED,
        payload: {}
    }
}

const createRequest = (formControls: FormControls, types: Array<WarehouseItemType>, id?: string): WarehouseItemRequest => {
    const key = (formControls.type as SelectInput).key
    const type: WarehouseItemType = types.find(type => type.code == key)!

    const item: WarehouseItemRequest = {
        _id: id,
        name: formControls.name.value,
        cost: parseFloat(formControls.cost.value),
        amount: parseFloat(formControls.amount.value),
        unit: formControls.unit.value,
        type: type._id
    };

    return item
};

export const validateForm = (name: string, value: string, formControls: FormControls) => {
    return (dispatch: Dispatch<AppActions>) => {
        const updatedFormControls: FormControls = updateFormControls(name, value, formControls)
        const isFormValid = validateFormControls(updatedFormControls)

        dispatch(formValidation(isFormValid, updatedFormControls))
    }
}

export const validateFromSelect = (name: string, key: string, value: string, formControls: FormControls) => {
    return (dispatch: Dispatch<AppActions>) => {
        const updatedFormControls: FormControls = updateFormControlsSelect(name, key, value, formControls)
        const isFormValid = validateFormControls(updatedFormControls)

        dispatch(formValidation(isFormValid, updatedFormControls))
    }
}

const formValidation = (isFormValid: boolean, formControls: FormControls): AppActions => {
    return {
        type: Actions.VALIDATE_FORM,
        payload: {
            isFormValid: isFormValid,
            formControls: formControls
        }
    }
}

export const clearReducerState = (): AppActions => {
    return {
        type: Actions.CLEAR_REDUCER_STATE,
        payload: {}
    }
}