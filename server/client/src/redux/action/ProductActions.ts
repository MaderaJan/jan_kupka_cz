import { Dispatch } from "redux"
import { AppActions } from "../../model/actions/AppActions"
import axios from "axios"
import { ProductType } from "../../model/data/ProductType"
import { Actions } from "../../model/actions/Actions"
import { BaseProductType } from "../../model/data/ProductTypes"
import { FormControls } from "../../model/formcontrols/FormControls"
import { updateFormControls, updateFormControlsMultiSelect, validateFormControls, updateFormControlsSelect } from "../../util/FormValidator"
import strings from "../../resources/strings"
import { ImageFile } from "../../model/data/ImageFile"
import { WarehouseItem } from "../../model/data/WarehouseItem"
import { MultiSelectInput, SelectInput, FormTextInput } from "../../model/formcontrols/FormTextInput"
import ProductApi from "../../api/ProductApi"
import { Product, ProductResponse } from "../../model/data/Product"
import { MinLength, GreaterThan, MinSelectedItems } from "../../model/formcontrols/ValidationRule"
import { WarehouseItemType } from "../../model/data/WarehouseItemType"
import ApiFactory from "../../api/ApiFactory"
import WarehouseApi from "../../api/WarehouseApi"

export const initData = () => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        const productApi = apiFactory.get(ProductApi.name) as ProductApi
        const warehouseApi = apiFactory.get(WarehouseApi.name) as WarehouseApi

        const productTypesRequest = productApi.getProductTypes()
        const itemTypesRequest = warehouseApi.getAllWarehouseItems()

        return axios.all([productTypesRequest, itemTypesRequest])
            .then(axios.spread((...responses) => {
                const producTypes: Array<ProductType> = responses[0].data
                const allItems: Array<WarehouseItem> = responses[1].data
                dispatch(initDataFetched(producTypes, allItems))
            }))
            .catch(err => console.log(err))
    }
}

const initDataFetched = (types: Array<ProductType>, allItems: Array<WarehouseItem>): AppActions => {
    const productTypeString = strings.product_type
    const typesWithNames = types.map(item => {
        item.name = productTypeString.find(type => type.code === item.type)!.name
        return item
    })

    return {
        type: Actions.PRODUCT_INIT_DATA_FETCHED,
        payload: {
            types: typesWithNames,
            threads: allItems.filter(item => item.type.code == "thread"),
            leathers: allItems.filter(item => item.type.code == "leather"),
            colors: allItems.filter(item => item.type.code == "color")
        }
    }
}

export const fetchProduct = (id: string) => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        const productApi = apiFactory.get(ProductApi.name) as ProductApi

        productApi.getProductById(id)
            .then(res => {
                const productResponse: ProductResponse = res.data
                const formControls = mapProductToFormControls(productResponse)
                const isFormValid = validateFormControls(formControls)
                const price = calculatePrice(getState(), formControls, isFormValid)
                const images = productResponse.images.map((img: string, index: number) => {
                    return new ImageFile(index, img, false)
                })

                dispatch(productFetched(formControls, isFormValid, id, price, images))
            })
            .catch(err => console.log(err))
    }
}

const mapProductToFormControls = (product: ProductResponse): FormControls => {
    const productName = strings.product_type.find(type => type.code === product.type.type)!.name

    const leatherKeys = product.leathers.map(leather => leather._id)
    const leathersName = product.leathers.map(leather => leather.name)

    const colorKeys = product.colors.map(color => color._id)
    const colorsName = product.colors.map(color => color.name)

    const threadKeys = product.threads.map(thread => thread._id)
    const threadsName = product.threads.map(thread => thread.name)

    return {
        name: new FormTextInput(new MinLength(3), product.name),
        description: new FormTextInput(new MinLength(3), product.description),
        type: new SelectInput(new MinLength(1), product.type._id, productName),
        avgWorkTime: new FormTextInput(new GreaterThan(0), product.avgWorkTime + ""),
        tools: new FormTextInput(new GreaterThan(0), product.tools + ""),
        toolsWareMarge: new FormTextInput(new GreaterThan(0), product.toolsWareMarge + ""),
        sellPrice: new FormTextInput(new GreaterThan(0), product.sellPrice + ""),
        colorConsumption: new FormTextInput(new GreaterThan(0), product.colorConsumption + ""),
        leatherConsumption: new FormTextInput(new GreaterThan(0), product.leatherConsumption + ""),
        threadConsumption: new FormTextInput(new GreaterThan(0), product.leatherConsumption + ""),
        selectedLeathers: new MultiSelectInput(new MinSelectedItems(1), leatherKeys, leathersName),
        selectedThreads: new MultiSelectInput(new MinSelectedItems(1), threadKeys, threadsName),
        selectedColors: new MultiSelectInput(new MinSelectedItems(1), colorKeys, colorsName),
    }
}

const productFetched = (formControls: FormControls, isFormValid: boolean, id: string, basePrice: string, images: Array<ImageFile>): AppActions => {
    return {
        type: Actions.PRODUCT_FETCHED,
        payload: {
            formControls, isFormValid, id, basePrice, images
        }
    }
}

export const validateForm = (name: string, value: string, formControls: FormControls) => {
    return (dispatch: Dispatch<AppActions>, getState: any) => {
        const updatedFormControls: FormControls = updateFormControls(name, value, formControls)
        const isFormValid = validateFormControls(updatedFormControls)
        const price = calculatePrice(getState(), formControls, isFormValid)

        dispatch(formValidation(isFormValid, updatedFormControls, price))
    }
}

export const validateFromSelect = (name: string, key: string, value: string, formControls: FormControls) => {
    return (dispatch: Dispatch<AppActions>, getState: any) => {
        const updatedFormControls: FormControls = updateFormControlsSelect(name, key, value, formControls)
        const isFormValid = validateFormControls(updatedFormControls)
        const price = calculatePrice(getState(), formControls, isFormValid)

        dispatch(formValidation(isFormValid, updatedFormControls, price))
    }
}

export const validateFormMulti = (selectedOption: any, action: any, formControls: FormControls) => {
    return (dispatch: Dispatch<AppActions>, getState: any) => {
        const updatedFormControls: FormControls = updateFormControlsMultiSelect(selectedOption, action, formControls)
        const isFormValid = validateFormControls(updatedFormControls)
        const price = calculatePrice(getState(), formControls, isFormValid)

        dispatch(formValidation(isFormValid, updatedFormControls, price))
    }
}

const formValidation = (isFormValid: boolean, formControls: FormControls, basePrice: string): AppActions => {
    return {
        type: Actions.VALIDATE_FORM,
        payload: {
            isFormValid: isFormValid,
            formControls: formControls,
            basePrice: basePrice
        }
    }
}

export const calculatePrice = (state: any, formControls: FormControls, isFormValid: boolean): string => {
    if (!isFormValid) return strings.no_price

    const leathers = state.productReducer.leathers as Array<WarehouseItem>
    const colors = state.productReducer.colors as Array<WarehouseItem>
    const threads = state.productReducer.threads as Array<WarehouseItem>

    const selectedLeathersIds = (formControls.selectedLeathers as MultiSelectInput).keys
    const selectedThreadsIds = (formControls.selectedThreads as MultiSelectInput).keys
    const selectedColorsIds = (formControls.selectedColors as MultiSelectInput).keys

    console.log("leather:" + leathers)
    console.log("thread:" + colors)

    let minLeather =
        getMinimumPrice(leathers, selectedLeathersIds) * formControls.leatherConsumption.value;

    let minThread =
        getMinimumPrice(threads, selectedThreadsIds) * formControls.threadConsumption.value;

    let minColor =
        getMinimumPrice(colors, selectedColorsIds) * formControls.colorConsumption.value;

    let price = Number(formControls.avgWorkTime.value) * (270 / 60) +
        minLeather +
        minThread +
        minColor +
        Number(formControls.tools.value);

    console.log("leather:" + minLeather)
    console.log("thread:" + minThread)
    console.log("color:" + minColor)
    console.log("warge" + (1 + formControls.toolsWareMarge.value / 100))

    price *= 1 + formControls.toolsWareMarge.value / 100;

    return Math.round(price).toString()
}

const getMinimumPrice = (allTypes: Array<WarehouseItem>, selectedTypes: Array<string>): number => {
    const mappedTypes = allTypes.filter(item =>
        selectedTypes.some(id => id === item._id)
    );

    let minPrice = Number.MAX_SAFE_INTEGER;
    mappedTypes.forEach(p => {
        if (p.cost <= minPrice) {
            minPrice = p.cost;
        }
    });

    return minPrice;
};

export const addImage = (images: Array<ImageFile>, imageData: string, imageFile: File): AppActions => {
    const image = new ImageFile(new Date().getMilliseconds(), imageData, false, imageFile)
    const updatedImages = [...images]
    updatedImages.push(image)

    return {
        type: Actions.PRODUCT_IMAGE_CHANGED,
        payload: updatedImages
    }
}

export const removeImage = (images: Array<ImageFile>, id: number) => {
    const updatedImages = images.filter(img => img.id != id)

    return {
        type: Actions.PRODUCT_IMAGE_CHANGED,
        payload: updatedImages
    }
}

export const markImageAsMain = (images: Array<ImageFile>, id: number) => {
    const updatedImages = [...images]
    updatedImages.forEach(img => {
        img.isMain = img.id === id
    })

    return {
        type: Actions.PRODUCT_IMAGE_CHANGED,
        payload: updatedImages
    }
}

export const createNewProduct = (formControls: FormControls, images: Array<ImageFile>, isInEditMode: boolean) => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        const productId = getState().productReducer.id
        const basePrice = getState().productReducer.basePrice
        const formData = createProductFormData(formControls, images, basePrice, productId);

        const productApi = apiFactory.get(ProductApi.name) as ProductApi
        
        const request = isInEditMode ? productApi.updateProduct(formData) : productApi.createProduct(formData)
        return request
            .then(res => dispatch(productCreated()))
            .catch(err => console.log(err));
    }
};

const productCreated = (): AppActions => {
    return {
        type: Actions.PRODUCT_CREATED,
        payload: {}
    }
}

const createProductFormData = (formControls: FormControls, images: Array<ImageFile>, basePrice: string, productId?: string) => {
    const formData = new FormData()

    const type = (formControls.type as SelectInput).key
    const leathers = (formControls.selectedLeathers as MultiSelectInput).keys
    const threads = (formControls.selectedThreads as MultiSelectInput).keys
    const colors = (formControls.selectedColors as MultiSelectInput).keys

    formData.append("name", formControls.name.value);
    formData.append("description", formControls.description.value);
    formData.append("type", type);
    formData.append("avgWorkTime", formControls.avgWorkTime.value);
    formData.append("colorConsumption", formControls.colorConsumption.value);
    formData.append("toolsWareMarge", formControls.toolsWareMarge.value);
    formData.append("tools", formControls.tools.value);
    formData.append("leatherConsumption", formControls.leatherConsumption.value);
    formData.append("threadConsumption", formControls.threadConsumption.value);
    formData.append("sellPrice", formControls.sellPrice.value);
    formData.append("basePrice", basePrice);
    
    threads.forEach(thread => formData.append("threads", thread))
    colors.forEach(color => formData.append("colors", color))
    leathers.forEach(leather => formData.append("leathers", leather))
    images.forEach((img, index) => {
        if (img.isMain) {
            formData.append("logoImageIndex", index + "")
        }
        if (img.imageFile != null) {
            // is new image from file
            formData.append("productPhotos", img.imageFile)
        } else {
            // is image from API
            formData.append("productPhotosUrls", img.imageData)
        }
    })

    if (productId) {
        formData.append("id", productId)
    }

    return formData;
}

export const clearReducerState = (): AppActions => {
    return {
        type: Actions.CLEAR_REDUCER_STATE,
        payload: {}
    }
}