import { Dispatch } from "redux"
import { AppActions } from "../../model/actions/AppActions"
import { Actions } from "../../model/actions/Actions"
import { Product, StatusDto, ChangeStatusesRequest } from "../../model/data/Product"
import ProductApi from "../../api/ProductApi"
import ApiFactory from "../../api/ApiFactory"

export const fetchProducts = () => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        const productApi = apiFactory.get(ProductApi.name) as ProductApi

        return productApi.getAllProducts()
            .then(res => {
                dispatch(products(res.data))
            })
            .catch(err => console.log(err))
    }
}

const products = (items: Array<Product>): AppActions => {
    return {
        type: Actions.PRODUCT_LIST_FETCHED,
        payload: items
    }
}

export const deleteProduct = (id: string) => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        const productApi = apiFactory.get(ProductApi.name) as ProductApi

        return productApi.deleteProdutById(id)
            .then(res => dispatch(productDeleted(id)))
            .catch(err => console.log(err));
    }
}

const productDeleted = (id: string): AppActions => {
    return {
        type: Actions.PRODUCT_LIST_ITEM_DELETED,
        payload: id
    }
}

export const changeProductStatus = (id: string, status: string) => {
    return (dispatch: Dispatch<AppActions>, getState: any) => {
        const products = getState().productListReducer.items as Array<Product>
        const productIndex = products.findIndex(product => product._id == id)
        products[productIndex].status = status

        const statusDtos = getState().productListReducer.changedStatuses as Array<StatusDto>
        const statusDtosIndex = statusDtos.findIndex(statusDto => statusDto.id == id)
        if (statusDtosIndex == -1) {
            const statusDto: StatusDto = { id: id, status: status }
            statusDtos.push(statusDto)
        } else {
            statusDtos[statusDtosIndex].status = status
        }

        dispatch(productStatusChanged(products, statusDtos))
    }
}

const productStatusChanged = (updatedProducts: Array<Product>, updatedStatuses: Array<StatusDto>): AppActions => {
    return {
        type: Actions.PRODUCT_STATUS_CHANGED,
        payload: {
            products: updatedProducts,
            statuses: updatedStatuses
        }
    }
}

export const updateProductStatuses = () => {
    return (dispatch: Dispatch<AppActions>, getState: any, apiFactory: ApiFactory) => {
        dispatch(contentLoading())

        const productApi = apiFactory.get(ProductApi.name) as ProductApi
        const statuses = getState().productListReducer.changedStatuses
        const request: ChangeStatusesRequest = { statuses: statuses }

        return productApi.postUpdateStatuses(request)
            .then(res => {
                dispatch(statusesUpdated())
            })
            .catch(err => console.log(err))
    }
}

const contentLoading = (): AppActions => {
    return {
        type: Actions.LOADING_STATE,
        payload: {}
    }
}

const statusesUpdated = (): AppActions => {
    return {
        type: Actions.PRODUCT_STATUS_UPDATED,
        payload: {}
    }
}


