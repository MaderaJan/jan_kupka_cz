import { Dispatch } from "react"
import { Actions } from "../../model/actions/Actions"
import { AppActions } from "../../model/actions/AppActions"
import { authToken, loggedAdminUser } from "../../resources/constants"

export const fetchData = () => {
    return (dispatch: Dispatch<AppActions>) => {
        const userName = localStorage.getItem(loggedAdminUser)
        dispatch(dateFetched(userName == null ? "" : userName))
    }
}

const dateFetched = (userName: string): AppActions => {
    return {
        type: Actions.ADMIN_TOP_BAR_DATA_FECHTED,
        payload: userName
    }
}

export const logoutUser = () => {
    return (dispatch: Dispatch<AppActions>) => {
        localStorage.setItem(loggedAdminUser, "");
        localStorage.setItem(authToken, "");
        
        dispatch(userLoggedOut())
    }
}

const userLoggedOut = (): AppActions => {
    return {
        type: Actions.ADMIN_TOP_BAR_USER_LOGGED_OUT,
        payload: {}
    }
}

export const clearReducerState = (): AppActions => {
    return {
        type: Actions.CLEAR_REDUCER_STATE,
        payload: {}
    }
}

