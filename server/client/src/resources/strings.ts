import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
    cz: {
        // General
        remove: "Odstranit",

        // Admin Form
        login: "Přihlášení",
        login_in: "Přihlásit se",
        username: "Přihlašovací jméno",
        password: "Heslo",
        error_wrong_login: "Chybné přihlašovací údaje",
        error_unknown: "Nastala neočekáváná chyba",

        // SideBar
        dashboard: "Přehled",
        products: "Produkty",
        warehouse: "Sklad",
        gallery: "Galerie",

        // TopBar
        user: "Uživatel ",
        logout: "Odhlásit ",

        // Product Type
        product_type: [
            {
                code: "belt",
                name: "Pásek"
            },
            {
                code: "cardholder",
                name: "Pásek k hodinkám"
            },
            {
                code: "watch_belt",
                name: "Pouzdro na karty"
            },
            {
                code: "wallet",
                name: "Peněženka"
            }
        ],


        // Product status
        product_status: [
            {
                code: "offline",
                name: "Nezobrazen"
            },
            {
                code: "online",
                name: "Zobrazen"
            },
        ],

        // Item Type
        item_type: [
            {
                code: "leather",
                name: "Kůže"
            },
            {
                code: "thread",
                name: "Niť"
            },
            {
                code: "color",
                name: "Barva"
            },
            {
                code: "other",
                name: "Jiné"
            }
        ],

        // WAREHOUSE
        name: "Název",
        price: "Cena",
        amount: "Množství",
        type: "Typ",
        choose_type: "Vyberte typ",
        unit: "Jednotka",
        choose_unit: "vyberte jednotku",
        action: "Akce",
        new_item: "Nová položka",
        create: "Vytvořit",
        edit: "Upravit",
        add_next: "Přidat další",

        // PRODUCT
        product: "Produkt",
        new_product: "Nový produkt",
        image: "Obrázek",
        work_time: "Doba práce",
        state: "Status",
        no_price: "N/A",
        photos: "Fotografie",
        general_info: "Obecné informace",
        avg_work_time: "Průměrná doba práce [min]",
        clue_and_more: "Lepidlo, vosk, líh, barva na hrany",
        tool_ware_marge: "Marže na nástroje [%]",
        sell_price: "Prodejní cena",
        leather: "Kůže",
        color: "Barva",
        thread: "Niť",
        kinds: "Druhy",
        consumption_dm2: "Spotřeba dm2",
        consumption_m: "Spotřeba [m]",
        color_ml: "Barva [ml]",
        product_description: "Popis produktu",
        price_info: "Informace o ceně",
        min_price: "Minimální cena",
        save_changes: "Uložit změny ",

        // DASHBOARD
        displayed: "Zobrazené",
        not_displayed: "Nezobrazené",
        total: "Celkem",

        // Error
        error_fill_field: "Pole musí být vyplněné",
        error_choose_one: "Vyberte jednu z možností"
    },
    en: {
        // General
        remove: "Odstranit",

        // Admin Form
        login: "Login",
        login_in: "Sing in",
        username: "Username",
        password: "Password",
        error_wrong_login: "Wrong username or password",
        error_unknown: "Server error",

        // SideBar
        dashboard: "Dashboard",
        products: "Products",
        warehouse: "Warehouse",
        gallery: "Gallery;",

        // TopBar
        use: "User ",
        logout: "Logout: ",

        // Product Type
        product_type: [
            {
                code: "belt",
                name: "Belt"
            },
            {
                code: "cardholder",
                name: "Cardholder"
            },
            {
                code: "watch_belt",
                name: "Watch belt"
            },
            {
                code: "wallet",
                name: "Wallet"
            }
        ],

        // Product status
        product_status: [
            {
                code: "offline",
                name: "Offline"
            },
            {
                code: "online",
                name: "Onlines"
            },
        ],

        // Item Type
        item_type: [
            {
                code: "leather",
                name: "Leather"
            },
            {
                code: "thread",
                name: "Thread"
            },
            {
                code: "color",
                name: "Color"
            },
            {
                code: "other",
                name: "Other"
            }
        ],

        // WAREHOUSE
        name: "Name",
        price: "Price",
        amount: "Amount",
        type: "Type",
        choose_type: "choose type",
        unit: "Unit",
        choose_unit: "choose unit",
        action: "Action",
        new_item: "New item",
        create: "Create",
        edit: "Edit",
        add_next: "Create next",

        // PRODUCT
        product: "Product",
        new_product: "New product",
        image: "Image",
        work_time: "Work time",
        state: "State",
        no_price: "N/A",
        photos: "Photos",
        general_info: "General info",
        avg_work_time: "Average work time [min]",
        clue_and_more: "Glue, wax, alcohol, edge paint",
        tool_ware_marge: "Tools ware marge [%]",
        sell_price: "Sell price",
        leather: "Leather",
        color: "Color",
        thread: "Thread",
        kinds: "Kinds",
        consumption_dm2: "Consumption dm2",
        consumption_m: "Consumption [m]",
        color_ml: "Consumption [ml]",
        product_description: "Product description",
        price_info: "Price info",
        min_price: "Minimum price",
        save_changes: "Save changes",

        // DASHBOARD
        displayed: "Online",
        not_displayed: "Offline",
        total: "Total",

        // Error
        error_fill_field: "Field must not be empty",
        error_choose_one: "Choose one of the options"
    }
});

export default strings;