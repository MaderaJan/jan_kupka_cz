enum PageNavigation {
    // ADMIN
    ADMIN_DASHBOARD = "/admin/dashboard",

    // ADMIN WAREHOUSE
    WAREHOUSE_LIST = "/admin/warehouse",
    WAREHOUSE_ITEM_DETAIL = "/admin/warehouse/detail",
    WAREHOUSE_ITEM_DETAIL_EDIT = "/admin/warehouse/detail/:id",

    // ADMIN PRODUCT
    PRODUCT_LIST = "/admin/products",
    PRODUCT_DETAIL = "/admin/products/detail",
    PRODUCT_DETAIL_EDIT = "/admin/products/detail/:id",

    // CONTACT
    CONTACT = "/contact",

    // SHOP
    SHOP = "/shop",

    // HOME
    HOME = "/",
}   

export default PageNavigation