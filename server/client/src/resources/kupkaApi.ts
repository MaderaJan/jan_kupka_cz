// import { process } from "process" TODO: typescript to nedovolí importovat :/

const BASE_URL = baseUrl();

export const endPoints = {

    // Auth
    login: BASE_URL + "administration-form",
    verify_token: BASE_URL + "verify-token",

    // Dashboard
    dashboard: BASE_URL + "dashboard",

    // Product
    products: BASE_URL + "product/list",
    products_list_status_update: BASE_URL + "product/list/update-status",
    product_types: BASE_URL + "product/types",
    product: BASE_URL + "product",

    // Warehouse
    warehouse_items: BASE_URL + "warehouse/item/list",
    warehouse_item_types: BASE_URL + "warehouse/item-types",
    warehouse_item: BASE_URL + "warehouse/item",

}

function baseUrl(): string {
    if (isDevelop()) {
        return "http://localhost:5000/";
    } else {
        return "http://jan-kupka.herokuapp.com:80/";
    }
}

function isDevelop(): boolean {
    return true
    // return !process.env.NODE_ENV || process.env.NODE_ENV === "development";
}