import React from 'react';
import ReactDom from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';    

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

ReactDom.render(<App/ >, document.getElementById('root'));
registerServiceWorker();