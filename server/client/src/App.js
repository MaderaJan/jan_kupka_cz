import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import axios from 'axios';

import { Provider } from "react-redux";
import store from "./redux/store/store.ts"

import AdminFormApp from './components/admin/adminForm/AdminFormApp';
import CustomerApp from './components/customer/CustomerApp';
import Admin from './components/admin/Admin';
import { authToken } from './resources/constants'

import 'jquery/dist/jquery.min.js';
import 'bootstrap/js/src/collapse.js';
import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {

    constructor() {
        super();        
    }

    render() { 
        return ( 
            <Provider store={store}>
                <BrowserRouter>
                    <Switch>
                        <Route path="/admin" component={Admin}/>
                        <Route path="/administrationform" component={AdminFormApp}/>
                        <Route exact path="/*" component={CustomerApp}/>
                    </Switch>
                </BrowserRouter>     
            </Provider>
         );
    }
}
 
export default App;