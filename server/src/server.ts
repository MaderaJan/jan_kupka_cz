import * as dotenv from "dotenv"

import App from "./app";
import AdministrationFormController from "./controllers/adminForm.controller";
import DashboardController from "./controllers/dashboard.controller";
import ProductController from "./controllers/product.controller";
import WarehouseController from "./controllers/warehouseItem.controller";

dotenv.config()

const app = new App(
	[
		new AdministrationFormController,
		new ProductController,
		new WarehouseController,
		new DashboardController
	]
)

app.listen()