import *as mongoose from 'mongoose';
import SuperUser from '../interface/superUser.interface';

const superUserSchema = new mongoose.Schema({
    name: { type: String, required: true },
    password: { type: String, required: true }
});

const superUserModel = mongoose.model<SuperUser & mongoose.Document>('super_user', superUserSchema);

export default superUserModel
