import * as mongoose from "mongoose";
import Product from "../interface/product.interface";

const productSchema = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  type: { type: mongoose.Schema.Types.ObjectId, ref: "product_types" },
  avgWorkTime: { type: Number, required: true },
  tools: { type: Number, required: true },
  colorConsumption: { type: Number, required: true },
  toolsWareMarge: { type: Number, required: true },
  sellPrice: { type: Number, required: true },
  leatherConsumption: { type: Number, required: true },
  threadConsumption: { type: Number, required: true },
  basePrice: { type: Number, required: true },
  leathers: [{ type: mongoose.Schema.Types.ObjectId, ref: "warehouse_item" }],
  threads: [{ type: mongoose.Schema.Types.ObjectId, ref: "warehouse_item" }],
  colors: [{ type: mongoose.Schema.Types.ObjectId, ref: "warehouse_item" }],
  images: { type: [String], required: false },
  logo: { type: String, required: false },
  status: { type: String, required: false },
});

const productModel = mongoose.model<Product & mongoose.Document>("product", productSchema);

export default productModel
