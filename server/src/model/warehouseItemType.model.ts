import * as mongoose from 'mongoose';
import WarehouseItemType from '../interface/warehouseItemType.interface';

const schema = new mongoose.Schema({
    code: { type: String, required: true }
});

const warehouseItemTypeModel = mongoose.model<WarehouseItemType & mongoose.Document>('warehouse_item_types', schema);

export default warehouseItemTypeModel
