import * as mongoose from "mongoose"
import WarehouseItem from "../interface/warehouseItem.interface";

const warehouseItemSchema = new mongoose.Schema({
    name: { type: String, required: true },
    cost: { type: Number, required: true },
    amount: { type: Number, required: true },
    type: { type: mongoose.Schema.Types.ObjectId, ref: "warehouse_item_types" },
    unit: { type: String, required: true },
});

const warehouseItemModel = mongoose.model<WarehouseItem & mongoose.Document>("warehouse_item", warehouseItemSchema)

export default warehouseItemModel