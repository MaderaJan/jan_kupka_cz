import * as mongoose from 'mongoose'
import ProductType from '../interface/productType.interface'

const productTypeSchema = new mongoose.Schema({
    type: { type: String, required: true }
})

const productTypeModel = mongoose.model<ProductType & mongoose.Document>('product_types', productTypeSchema)

export default productTypeModel