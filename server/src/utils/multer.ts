import * as multer from "multer";

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, process.env.UPLOAD_PATH);
	},
	filename: (req, file, cb) => {
		cb(null, (new Date().getTime()) + "_" + file.originalname);
	}
});

const upload = multer({ storage: storage });

export default upload