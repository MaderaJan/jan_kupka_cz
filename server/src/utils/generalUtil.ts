import * as process from "process";

export default function() {
    if (isDevelop()) {
        return "http://localhost:5000/";
    } else {
        return "http://jan-kupka.herokuapp.com:80/";
    }
}

function isDevelop() {
    return !process.env.NODE_ENV || process.env.NODE_ENV === "development";
}
