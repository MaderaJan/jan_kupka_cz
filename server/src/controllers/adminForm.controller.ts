import { Router, Request, Response } from 'express';
import * as HttpStatusCodes from "http-status-codes"

import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import BaseController from '../interface/baseController.interface';
import superUserModel from '../model/superUser.model';
import DataStoredInToken from '../interface/dataStoredInToken.interface';

class AdministrationFormController implements BaseController {
    path = "/administration-form"
    router = Router()

    constructor() {
        this.initRoutes()
    }

    initRoutes() {
        this.router.post(this.path, this.signInSuperUser)
    }

    private signInSuperUser = async (request: Request, response: Response) => {
        console.log("POST: administration-form")
        const password = request.body.password

        superUserModel.findOne({ name: request.body.name })
            .then(user => {
                if (user && bcrypt.compareSync(password, user.password)) {
                    console.log(user);
                    this.createTokenResponse(response, user._id, user.name);
                } else {
                    response.status(HttpStatusCodes.FORBIDDEN).json();
                }
            })
            .catch((err: Error) => {
                console.log(err);
                response.status(HttpStatusCodes.BAD_REQUEST).json('Error: ' + err)
            });
    }

    private createTokenResponse(response: Response, userId: string, userName: string) {
        const data: DataStoredInToken = { _id: userId }

        const token: string = jwt.sign(data, process.env.TOKEN_SECRET)
        response.status(HttpStatusCodes.OK).json({ token: token, user: userName })
    }
}

export default AdministrationFormController
