import { Router, Request, Response } from "express"
import * as HttpStatusCodes from "http-status-codes"

import warehouseItemModel from "./../model/warehouseItem.model";
import warehouseItemTypeModel from "./../model/warehouseItemType.model";

import BaseController from "../interface/baseController.interface";
import validateAuthTokenMiddleware from "../middleware/validateAuthToken.middleware";

class WarehouseController implements BaseController {
    path = "/warehouse"
    router = Router()

    constructor() {
        this.initRoutes()
    }

    initRoutes() {
        this.router.all(`${this.path}/*`, validateAuthTokenMiddleware)

        this.router.get(`${this.path}/item`, this.getWarehouseItemById)
        this.router.get(`${this.path}/item/list`, this.getAllWareHouseItems)
        this.router.get(`${this.path}/item-types`, this.getAllWarehouseItemTypes)
        this.router.post(`${this.path}/item`, this.postWarehouseItem)
        this.router.put(`${this.path}/item`, this.putWarehouseItem)
        this.router.delete(`${this.path}/item`, this.deleteItemById)
    }

    private getWarehouseItemById = async (request: Request, response: Response) => {
        console.log("GET /warehouse/item");

        warehouseItemModel.findById(request.query.id)
            .populate("type")
            .then(items => response.status(HttpStatusCodes.OK).json(items))
            .catch(err => {
                console.log(err);
                response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR);
            });
    }

    private getAllWareHouseItems = async (_: Request, response: Response) => {
        console.log("GET: warehouse/items");

        warehouseItemModel.find({})
            .populate("type")
            .then(items => {
                console.log(items);
                response.status(HttpStatusCodes.OK).json(items)
            })
            .catch(err => {
                console.log(err);
                response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR);
            });
    }

    private getAllWarehouseItemTypes = async (_: Request, response: Response) => {
        console.log("GET /warehouse/item-types");

        warehouseItemTypeModel.find({})
            .then(types => {
                console.log(types);
                response.status(HttpStatusCodes.OK).json(types)
            })
            .catch(err => {
                console.log(err);
                response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR);
            })
    }

    private postWarehouseItem = async (request: Request, response: Response) => {
        console.log("POST: warehouse/item");
        const item = new warehouseItemModel(request.body.item);

        item.save()
            .then(_ => response.status(HttpStatusCodes.OK).json())
            .catch(err => {
                console.log(err)
                response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR)
            })
    }

    private putWarehouseItem = async (request: Request, response: Response) => {
        console.log("PUT: warehouse/item");

        const item = new warehouseItemModel(request.body.item);
        console.log(item)

        warehouseItemModel.replaceOne({ _id: item._id }, item)
            .then(_ => response.status(HttpStatusCodes.OK).json())
            .catch(err => {
                console.log(err)
                response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR)
            })
    }

    private deleteItemById = async (request: Request, response: Response) => {
        console.log("DELETE: /warehouse/item");

        warehouseItemModel.deleteOne({ _id: request.query.id })
            .then( _ => response.status(HttpStatusCodes.OK).json())
            .catch(err => {
                console.log(err)
                response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR);
            });
    }
}

export default WarehouseController