import BaseController from "../interface/baseController.interface";
import DashboardInfo from "../interface/dashboardInfo.interface";
import validateAuthTokenMiddleware from "../middleware/validateAuthToken.middleware";
import productModel from "../model/product.model";
import warehouseItemModel from "../model/warehouseItem.model";
import * as HttpStatusCodes from "http-status-codes"
import { Router, Request, Response } from "express";

class DashboardController implements BaseController {
    path = "/dashboard"
    router = Router()

    constructor() {
        this.initRoutes()
    }

    initRoutes() {
        this.router.get(this.path, validateAuthTokenMiddleware, this.getDashboardData)
    }

    private getDashboardData = async (_: Request, response: Response) => {
        console.log("GET dashboard")

        // TODO: tento dotaz lze udělat paralelně
        const productOnline = await productModel.countDocuments({status: "online"})
        const productOffline = await productModel.countDocuments({status: "offline"})
        const warehouseItemsCounts = await warehouseItemModel.countDocuments({})

        const responseModel: DashboardInfo = {
            productOffline: productOffline,
            productOnline: productOnline,
            warehouseItemsCounts: warehouseItemsCounts
        }

        response.status(HttpStatusCodes.OK).json(responseModel)
    }
}

export default DashboardController