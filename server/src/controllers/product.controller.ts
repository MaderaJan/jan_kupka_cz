import productModel from "../model/product.model";
import productTypeModel from "../model/productType.model";

import baseUrl from "../utils/generalUtil";

import * as HttpStatusCodes from "http-status-codes"
import { Router, Request, Response } from "express";
import BaseController from "../interface/baseController.interface";

import upload from "../utils/multer";
import * as fs from "fs"
import validateAuthTokenMiddleware from "../middleware/validateAuthToken.middleware";
import StatusDto from "../interface/statusDto";

class ProductController implements BaseController {
	path = "/product";
	router = Router();

	constructor() {
		this.initRoutes()
	}

	initRoutes() {
		this.router.all(`${this.path}/*`, validateAuthTokenMiddleware)

		this.router.get(`${this.path}/list`, this.getAllProducts)
		this.router.get(`${this.path}/types`, this.getProductTypes)
		this.router.get(`${this.path}`, this.getProductById)
		this.router.post(this.path, upload.array("productPhotos"), this.postProduct)
		this.router.post(`${this.path}/list/update-status`, this.postUpdateProductStatuses)
		this.router.put(this.path, upload.array("productPhotos"), this.putProduct)
		this.router.delete(`${this.path}`, this.deleteProduct)
	}

	private getAllProducts = async (_: Request, response: Response) => {
		console.log("GET: products");

		productModel.find({})
			.populate("type")
			.then(products => {
				const url = baseUrl();

				const mappedProducts = products.map(product => {
					product.images.map(img => {
						return url + img;
					});

					return product;
				});

				console.log(mappedProducts);
				response.status(HttpStatusCodes.OK).json(mappedProducts);
			})
			.catch(err => {
				console.log(err);
				response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR);
			});
	}

	private getProductTypes = async (_: Request, response: Response) => {
		console.log("GET: /product/types");

		productTypeModel.find({})
			.then(types => {
				if (types) {
					console.log("prázndé");
				}

				console.log("sucess" + types);
				response.status(HttpStatusCodes.OK).json(types);
			})
			.catch(err => {
				console.log("error: " + err);
				response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json(err);
			});
	}

	private getProductById = async (request: Request, response: Response) => {
		console.log("GET: product");

		productModel.findById({ _id: request.query.id })
			.populate("type")
			.populate("leathers")
			.populate("threads")
			.populate("colors")
			.then(result => {
				console.log(result);
				response.status(HttpStatusCodes.OK).json(result);
			})
			.catch(err => {
				console.log(err);
				response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR);
			});
	}

	private postProduct = async (request: Request, response: Response) => {
		console.log("POST: /product");

		const imagePaths: Array<string> = this.getImagePathFromFiles(request)
		const newProduct = this.createFromRequestBody(request, imagePaths);
		console.log(newProduct);

		newProduct
			.save()
			.then(_ => response.status(HttpStatusCodes.OK).json("OK"))
			.catch(err => {
				console.log(err);
				response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR);
			});
	}

	private putProduct = async (request: Request, response: Response) => {
		console.log("PUT: /product");
		console.log("IDčko" + request.body.id)

		const imagePaths: Array<string> = this.getImagePathFromFiles(request)
		const productPhotosUrls = request.body.productPhotosUrls

		if (Array.isArray(productPhotosUrls)) {
			productPhotosUrls.forEach(imgUrl => {
				imagePaths.push(imgUrl)
			})
		} else if (productPhotosUrls) {
			imagePaths.push(productPhotosUrls)
		}

		this.deleteUnUsedImages(request.body.id, imagePaths)
		const product = this.createFromRequestBody(request, imagePaths);

		productModel.replaceOne({ _id: product._id }, product)
			.then(_ => response.status(HttpStatusCodes.OK).json())
			.catch(err => {
				console.log(err);
				response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR);
			});
	}

	private createFromRequestBody(request: Request, imagePaths: Array<string>) {
		const body = request.body

		let logoImageUrl = this.getLogoImageUrl(imagePaths, body.logoImageIndex);

		return new productModel({
			_id: body.id,
			name: body.name,
			description: body.description,
			type: body.type,
			avgWorkTime: body.avgWorkTime,
			tools: body.tools,
			toolsWareMarge: body.toolsWareMarge,
			colorConsumption: body.colorConsumption,
			leatherConsumption: body.leatherConsumption,
			threadConsumption: body.threadConsumption,
			sellPrice: body.sellPrice,
			basePrice: body.basePrice,
			leathers: body.leathers,
			threads: body.threads,
			colors: body.colors,
			images: imagePaths,
			logo: logoImageUrl
		});
	}

	private getLogoImageUrl(imagePaths: string[], logoImageIndex?: number) {
		let logoImageUrl: string;
		if (logoImageIndex && logoImageIndex <= imagePaths.length) {
			logoImageUrl = imagePaths[logoImageIndex];
		}
		else if (logoImageUrl == null && imagePaths.length > 0) {
			logoImageUrl = imagePaths[0];
		}

		return logoImageUrl;
	}

	private getImagePathFromFiles(request: Request): Array<string> {
		let imagePaths: Array<string>;
		if (request.files && Array.isArray(request.files)) {
			imagePaths = request.files.map(file =>
				file.path.replace("public", "").toString()
			)
		}
		return imagePaths
	}

	private async deleteUnUsedImages(productId: string, newImages: Array<string>) {
		const allProductImages = (await productModel.findOne({ _id: productId })).images
		const imagesToDelete = allProductImages.filter(imgPath => !newImages.includes(imgPath));
		console.log(allProductImages)
		console.log(newImages)
		console.log("IMAGE -----------------> " + imagesToDelete.length)

		this.deleteImagesFromFolder(imagesToDelete)
	}

	private deleteProduct = async (request: Request, response: Response) => {
		console.log("DELETE: product");

		const productToDelete = await productModel.findOne({ _id: request.query.id })
		this.deleteImagesFromFolder(productToDelete.images)

		productModel.deleteOne({ _id: request.query.id })
			.then(_ => response.status(HttpStatusCodes.OK).json())
			.catch(err => {
				console.log(err);
				response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR);
			});
	}

	private deleteImagesFromFolder(images?: Array<string>) {
		images?.forEach(imgPath => {
			fs.unlinkSync("public/" + imgPath)
		})
	}

	private postUpdateProductStatuses = async (request: Request, response: Response) => {
		console.log("POST product/list/update-status")

		const statuses: Array<StatusDto> = request.body.statuses
		try {
			statuses.forEach(status => {
				this.updateStatus(status);
			})
			response.status(HttpStatusCodes.OK).json()
		} catch(ex) {
			response.status(HttpStatusCodes.CONFLICT)
		}		
	}

	private updateStatus = async (status: StatusDto) => {
		await productModel.updateOne({ _id: status.id }, {
			$set: {
				status: status.status
			}
		});
	}
}

export default ProductController