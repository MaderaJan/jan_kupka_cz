// Modules
import * as express from 'express';
import * as mongoose from 'mongoose';
import * as cors from 'cors';
import * as path from 'path';

// Https
import BaseController from './interface/baseController.interface';

class App {
	public app: express.Application;

	constructor(controllers: Array<BaseController>) {
		this.app = express()

		this.initDatabase()
		this.initMiddleWare()
		this.initControllers(controllers)
	}

	public listen() {
		const port = process.env.PORT || 5000;

		if (process.env.NODE_ENV == "production") {
			this.app.get('*', (req, res) => {
				res.sendFile(path.join(__dirname + '/client/build/index.html'));
			});
		}

		this.app.listen(port, function () {
			console.log(`Server runnning on port: ${port}`);
		});
	}

	private initDatabase() {
		const uri = `mongodb+srv://${process.env.MONGO_LOGIN}:${process.env.MONGO_PASSWORD}${process.env.MONGO_PATH}`
		mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });
		mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
		mongoose.connection.once('open', function () {
			console.log('DB connection successfuly made');
		});
	}

	private initMiddleWare() {
		this.app.use(express.static('public/'));
		this.app.use(express.static(path.join(__dirname, 'client/build')));
		this.app.use(express.json());
		this.app.use(cors());
	}

	private initControllers(controlers: Array<BaseController>) {
		controlers.forEach(controller => {
			this.app.use("/", controller.router);
		})
	}
}

export default App