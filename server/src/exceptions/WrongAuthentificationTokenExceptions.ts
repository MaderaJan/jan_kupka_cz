import HttpException from './HttpException';
import * as HttpStatusCodes from "http-status-codes"

class WrongAuthenticationTokenException extends HttpException {
    
    constructor() {
        super(HttpStatusCodes.UNAUTHORIZED, 'Wrong authentication token');
    }
}

export default WrongAuthenticationTokenException;
