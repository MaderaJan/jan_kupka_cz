import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import DataStoredInToken from '../interface/dataStoredInToken.interface';
import superUserModel from '../model/superUser.model';
import WrongAuthenticationTokenException from '../exceptions/WrongAuthentificationTokenExceptions';

const validateAuthTokenMiddleware = async (request: Request, _: Response, nextFunction: NextFunction) => {
    try {
        
        console.log(request.headers)


        const token = request.headers.authorization
        const superUserId = (jwt.verify(token, process.env.TOKEN_SECRET) as DataStoredInToken)._id

        const user = await superUserModel.findById(superUserId)
        if (user) {
            nextFunction()
        } else {
            nextFunction(new WrongAuthenticationTokenException())
        }
    } catch (error) {
        nextFunction(new WrongAuthenticationTokenException())
    }
}

export default validateAuthTokenMiddleware