interface DashboardInfo {
    productOnline: number,
    productOffline: number,
    warehouseItemsCounts: number
}

export default DashboardInfo