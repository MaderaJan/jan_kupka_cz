import WarehouseItemType from "./warehouseItemType.interface";

interface WarehouseItem {
    name: string,
    cost: number,
    amount: number,
    type: WarehouseItemType,
    unit: string
}

export default WarehouseItem