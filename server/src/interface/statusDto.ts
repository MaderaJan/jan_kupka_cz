interface StatusDto {
    id: string,
    status: string
}

export default StatusDto