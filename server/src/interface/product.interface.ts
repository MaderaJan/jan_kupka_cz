import ProductType from "./productType.interface";
import WarehouseItem from "./warehouseItem.interface";

interface Product {
  name: string,
  description: string,
  type: ProductType,
  avgWorkTime: number,
  tools: number,
  colorConsumption: number,
  toolsWareMarge: number,
  leatherConsumption: number,
  threadConsumption: number,
  sellPrice: number,
  basePrice: number,
  leathers: Array<WarehouseItem>,
  threads: Array<WarehouseItem>,
  colors: Array<WarehouseItem>,
  images?: [string],
  logo?: string,
  status?: string
}

export default Product
