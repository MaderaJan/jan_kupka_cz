import { Router } from "express"

interface BaseController {
    path: string,
    router: Router

    initRoutes()
}

export default BaseController